from pyramid.authentication import AuthTktAuthenticationPolicy
from pyramid.authorization import ACLAuthorizationPolicy
from pyramid.config import Configurator
from pyramid_mailer import mailer_factory_from_settings
from pyramid.session import UnencryptedCookieSessionFactoryConfig
from .security import groupfinder

import ldap3

def main(global_config, **settings):
	""" This function returns a Pyramid WSGI application.
    """
	# declarations
	session_factory = UnencryptedCookieSessionFactoryConfig('Ty6sIUe9')
	authn_policy = AuthTktAuthenticationPolicy('Ty6sIUe9', callback=groupfinder, hashalg='sha512')
	authz_policy = ACLAuthorizationPolicy()
	mailer_factory_from_settings(settings)

	config = Configurator(settings=settings,
						  root_factory='caes_labs.models.RootFactory')
	config.include('.models')
	config.include('.routes')
	config.include('pyramid_ldap3')

	# configurations
	config.set_session_factory(session_factory)
	config.set_authentication_policy(authn_policy)
	config.set_authorization_policy(authz_policy)

	# definir serveur LDAP
	config.ldap_setup(
		'ldap://194.57.123.20',
		bind='CAES_CNRS\ldapdude',
		passwd='ldapldap')

	config.ldap_set_login_query(
		base_dn='CN=Users,DC=caes-cnrs,DC=org',
		filter_tmpl='(sAMAccountName=%(login)s)',
		scope=ldap3.LEVEL)

	config.ldap_set_groups_query(
		base_dn='CN=Users,DC=caes-cnrs,DC=org',
		filter_tmpl='(&(objectCategory=group)(member=%(userdn)s))',
		scope=ldap3.SUBTREE,
		cache_period=600)           

	config.scan()
	return config.make_wsgi_app()
