# -*- coding: utf8 -*-
from pyramid_layout.layout import layout_config
from .tools import *
from .views.default import *

@layout_config(template='caes_labs:templates/layouts/global_layout.pt')
class GlobalLayout(object):
	page_title = u"Caes gestion"

	def __init__(self, context, request):
			self.context = context
			self.request = request
			self.home_url = request.application_url
	
	def to_euro(self, x):
		return to_euro(x)
	
	def to_percent(self, x):
		return to_percent(x)
	
	def isAdmin(self):
		is_ok = isAdmin(self.request)
		return is_ok
		
	def isGestionnaire(self):
		is_ok = isGestionnaire(self.request)
		return is_ok
	
	def isSac(self):
		is_ok = isSac(self.request)
		return is_ok

	def isSu(self):
		is_ok = isSu(self.request)
		return is_ok	
