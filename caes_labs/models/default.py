# -*- coding: utf8 -*-
from sqlalchemy import text
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import (
		scoped_session,
		sessionmaker,
		)

from zope.sqlalchemy import ZopeTransactionExtension, mark_changed

from datetime import *
import transaction
import random

def execute_query(request, query, params):
	"""Execute query and mark session as changed"""
	request.dbsession.execute(query, params)
	mark_changed(request.dbsession)
	transaction.commit()
	
def get_admin(request): 
	query = "select resp_saisie,nom,prenom,mel,matricule,niveau_web from p_user WHERE niveau_web='ADMIN' order by resp_saisie;"
	results = request.dbsession.execute(query).fetchall()
	return results		

def get_exercice():
	# Lire la date du jour pour décomposer en AAAA MM JJ
	today = date.today().strftime("%d/%m/%Y").split("/")
	(jour,mois,exercice) = (int(today[0]), int(today[1]), int(today[2]))		
	if jour >= 1 and mois >= 9:
		exercice = exercice +1
	return exercice

def get_last_exercice():
	# Lire la date du jour pour décomposer en AAAA MM JJ
	today = date.today().strftime("%d/%m/%Y").split("/")
	(jour,mois,exercice) = (int(today[0]), int(today[1]), int(today[2]))	
	if jour >= 1 and mois >= 11:
		exercice = exercice +1
	return [exercice-2,exercice-1,exercice]

def get_millesime():
	today = date.today().strftime("%d/%m/%Y").split("/")
	(jour,mois,millesime) = (int(today[0]), int(today[1]), int(today[2]))		
	if jour >= 1 and mois >= 11:
		millesime = millesime +1
	return millesime

def get_last_millesime():
	# Lire la date du jour pour décomposer en AAAA MM JJ
	today = date.today().strftime("%d/%m/%Y").split("/")
	(jour,mois,millesime) = (int(today[0]), int(today[1]), int(today[2]))	
	if jour >= 1 and mois >= 11:
		millesime = millesime +1
	return [millesime-2,millesime-1,millesime]	

def get_gestionnaire(request): 
	query = "select resp_saisie,nom,prenom,mel,matricule,niveau_web,etat_libel2 from p_user WHERE niveau_web is not null order by resp_saisie;"
	results = request.dbsession.execute(query).fetchall()
	return results

def get_gestionnaire_by_resp_saisie(request, resp_saisie): 
	query = "select * from p_user WHERE resp_saisie=:resp_saisie;"
	results = request.dbsession.execute(query, {'resp_saisie': resp_saisie}).first()
	return results

def get_web_tickets(request, id):
	query = "SELECT * FROM web_tickets WHERE id=:id;"
	results = request.dbsession.execute(query, {'id': id})
	return results.first()

def update_carte_familiale(request, matricule, new_values):
	s = ''
	for param in new_values.keys():
		if s:
			s += ",%s=:%s" % (param, param)
		else:
			s = "%s=:%s" % (param, param)
	new_values['exercice'] = get_exercice()
	new_values['matricule'] = matricule
	query = u"UPDATE carte_famil_d SET %s WHERE matricule=:matricule and exercice=:exercice;" % s
	execute_query(request, query, new_values)

	 		
def update_carte_familiale_td(request, matricule,tdf,tde):
	query = u"UPDATE carte_famil_d SET td_fam=:td_fam, td_enf=:td_enf WHERE matricule=:matricule and exercice=:exercice;"
	execute_query(request, query, {'matricule':matricule,'exercice':get_exercice(), 'td_fam':tdf, 'td_enf':tde })
	
def update_carte_familiale_non_rfr(request, matricule, non_rfr):	
	if non_rfr==1:
		query = """UPDATE carte_famil_d  SET revenu_ppal_od=999999,qf= 999999, revenu_conjoint=0, non_rfr=:non_rfr, date_non_rfr=NOW()
		WHERE matricule=:matricule and exercice=:exercice;"""
	else:
		query = """UPDATE carte_famil_d  SET non_rfr=:non_rfr, date_non_rfr=NOW()
		WHERE matricule=:matricule and exercice=:exercice;"""
	
	execute_query(request, query, {'matricule':matricule,'exercice':get_exercice(), 'non_rfr':non_rfr})		
	
def update_donnant_d(request, matricule, new_values):
	s = ''
	for param in new_values.keys():
		if s:
			s += ",%s=:%s" % (param, param)
		else:
			s = "%s=:%s" % (param, param)
	new_values['matricule'] = matricule
	query = u"UPDATE donnant_d SET %s WHERE matricule=:matricule ;" % s
	execute_query(request, query, new_values)

def update_gestionnaire(request, resp_saisie, niveau_web):
	query = u"UPDATE p_user SET niveau_web=:niveau_web WHERE resp_saisie=:resp_saisie and niveau_web is not null ;"
	execute_query(request, query, {'resp_saisie':resp_saisie,'niveau_web':niveau_web})

def update_dd_justifs(request, id, new_values, logged_in):
	new_values['resp_saisie'] = logged_in
	s = ''
	
	for param in new_values.keys():
		if s:
			s += ",%s=:%s" % (param, param)
		else:
			s = "%s=:%s" % (param, param)	
	new_values['id'] = id
	query = u"UPDATE dd_justifs SET %s WHERE id=:id;"% s
	execute_query(request, query, new_values)

def update_dd_justifs_lieu_stockage(request, id, lieu_stockage):
	query = u"UPDATE dd_justifs SET lieu_stockage=:lieu_stockage WHERE id=:id;"
	execute_query(request, query, {'id':id,'lieu_stockage':lieu_stockage})	
	
def get_enfance(request, matricule):
	"""Lire les dossiers subvention administrative du matricule"""
	query = """
SELECT dossier, fami_benef, lieu, date_deb, date_fin, nbre_jours, mtfact, mtce, date_remchq FROM mvt_enfance 
WHERE stece='CE' AND cion = 'ENFANCE' AND matricule=:matricule 
ORDER BY date_deb DESC ;"""
	results = request.dbsession.execute(query, {'matricule': matricule})
	return results.fetchall()

def update_parametre_cesu(request, exercice, debut_v1,fin_v1,debut_v2,fin_v2,date_millesime,mt_plafond,tranche_sup):		
	query = u"""UPDATE p_bareme b inner join p_bareme_financier f on b.stece=f.stece and b.exercice = f.exercice and b.code = f.code
    SET b.debut_v1 = :debut_v1, b.fin_v1=:fin_v1, b.debut_v2=:debut_v2, b.debut_v2=:debut_v2, b.fin_v2=:fin_v2, b.date_millesime=:date_millesime, f.mt_plafond=:mt_plafond, f.tranche_sup=:tranche_sup
    WHERE b.stece='CE' and b.exercice=:exercice and b.code in ('00050301','00050302','00050303','00050304','00050305','00050306');""" 
	execute_query(request, query, {'exercice': exercice, 'debut_v1': debut_v1, 'fin_v1': fin_v1, 'debut_v2': debut_v2, 'fin_v2': fin_v2, 'date_millesime': date_millesime, 'mt_plafond': mt_plafond, 'tranche_sup': tranche_sup})	

def update_parametre_cheque_vacance(request, exercice, debut_v1,fin_v1,tranche_sup):		
	query = u"""UPDATE p_bareme b inner join p_bareme_financier f on b.stece=f.stece and b.exercice = f.exercice and b.code = f.code
	    SET b.debut_v1 = :debut_v1, b.fin_v1=:fin_v1, f.tranche_sup=:tranche_sup
	    WHERE b.stece='CE' and b.exercice=:exercice and b.code = '00040101';""" 
	execute_query(request, query, {'exercice': exercice, 'debut_v1': debut_v1, 'fin_v1': fin_v1, 'tranche_sup': tranche_sup})	

def email_pwd(request, matricule):
	lien = generer_lien()
	query = "UPDATE donnant_d SET mdp_oublie=:lien, mdp_oublie_date=now() WHERE matricule=:matr;"
	execute_query(request, query, {'lien':lien, 'matr':matricule})
	return lien

def is_carte_famil_d(request, matricule, exercice):
	if exercice == '':
		query = "SELECT matricule FROM carte_famil_d WHERE matricule=:matr;"
		results = request.dbsession.execute(query, {'matr': matricule}).fetchall()
	else :
		query = "SELECT matricule FROM carte_famil_d WHERE matricule=:matr AND exercice=:ex;"
		results = request.dbsession.execute(query, {'matr': matricule, 'ex': exercice}).fetchall()
	return len(results) > 0
	
def is_member(request, userid):
	query = "select resp_saisie, Niveau, mel from p_user where resp_saisie=:userid"
	results = request.dbsession.execute(query, {'userid': userid}).first()
	return results

def insert_web_revenus(request, matricule):	
	if is_web_revenus_valide(request, matricule) == False:
		query = """
	INSERT INTO web_revenus 
	(matricule,exercice,
	nbpartcaes,nbpartcaes2,nbpartfisc,revenu_ppal_od,revenu_conjoint,indice_salarial,
	cod_region,cod_employeur,cod_qualif_pro,etat)
	(select 
	:matricule,:exercice,
	c.nbpartcaes,c.nbpartcaes2,c.nbpartfisc,c.revenu_ppal_od,c.revenu_conjoint,c.indice_salarial,
	d.cod_region,d.cod_employeur,d.cod_qualif_pro,10 from carte_famil_d c inner join donnant_d d on d.matricule=c.matricule  where c.matricule=:matricule and c.exercice=:exercice);"""			
		execute_query(request, query, {'matricule': matricule,'exercice': get_exercice()})
	
def is_web_revenus_valide(request, matricule):
	query = """
SELECT * FROM web_revenus 
WHERE matricule=:matricule AND exercice=:exercice;"""
	results = request.dbsession.execute(query, {'matricule': matricule, 'exercice': get_exercice()}).fetchall()
	return len(results)>0

def get_group(request, login):
	query = "select niveau_web from p_user where resp_saisie=:login"
	results = request.dbsession.execute(query, {'login': login}).first()
	return results
	
def is_ddjustifs(request, matricule, exercice, typeFic, code, service):
	query = """
SELECT * FROM dd_justifs 
WHERE matricule=:matr AND exercice=:exercice AND type=:type AND code=:code AND service=:service;"""
	results = request.dbsession.execute(query, {'matr': matricule, 'exercice': exercice, 'type': typeFic, 'code': code, 'service': service}).fetchall()
	return len(results)>0

def get_dossier_bynum(request, stece, dossier):
	"""Lire mvt_consom pour recuperer le solde"""
	query = "SELECT mtsolde FROM mvt_consom WHERE stece=:stece AND dossier=:dossier;"
	results = request.dbsession.execute(query, {'stece': stece, 'dossier': dossier})
	return results.fetchall() 
def get_web_facture(request, stece, dossier):
	"""Lire web_facture pour avoir le solde du dossier"""
	query = "SELECT mtsolde FROM web_facture WHERE stece=:stece AND dossier=:dossier;"
	results = request.dbsession.execute(query, {'stece': stece, 'dossier': dossier})
	return results.fetchall() 
def add_carte_famil_d_vide(request, exercice, matricule, indice, id_web):
	ex2 = exercice-2

	# Dans le cas de création de nouvelle fiche,	
	if is_carte_famil_d(request, matricule,'') == False:
		if indice == 9999:	
			query = """
INSERT INTO carte_famil_d(exercice, matricule, nompre, resp_maj, indice_salarial, anneerevenus)
(select  :exercice, matricule, nompre, 'WEB', :indice, :ex2  from donnant_d where matricule = :matricule);"""
			execute_query(request, query, {'exercice': exercice, 'matricule':matricule, 'indice':indice, 'ex2':ex2})
		else:
			query = """
INSERT INTO carte_famil_d(exercice, matricule, nompre, resp_maj, cod_situfam, indice_salarial, anneerevenus)
(select  :exercice, matricule, concat(nom,',',prenom), 'WEB', cod_situfam , indice_majore, :ex2  from web_inscription where id_web = :id_web);"""
			execute_query(request, query, {'exercice': exercice, 'id_web':id_web, 'indice':indice, 'ex2':ex2})
	else:
		query = """
			INSERT INTO carte_famil_d(exercice, matricule, nompre, resp_maj, cod_situfam, indice_salarial, titulaire, bqdom, bic, iban, parent_isole, conge_parental, anneerevenus)
	(select  :exercice, :matricule, nompre, 'WEB', cod_situfam, indice_salarial, titulaire, bqdom, bic, iban, parent_isole, conge_parental,:ex2  from carte_famil_d where matricule = :matricule order by exercice DESC limit 1);"""
		execute_query(request, query, {'exercice': exercice, 'matricule':matricule, 'indice':indice, 'ex2':ex2})
		
def add_ddjustifs(request, matricule, type, code, libelle, resp_saisie, concerne, service, annuel, lieu_stockage, date_validite):
	query = """
	    INSERT INTO dd_justifs (matricule, exercice, type, code, libelle,  resp_saisie, service, annuel, lieu_stockage, date_validite)
	    VALUES (:matr,:exercice,:type,:code,:libelle,:resp_saisie,:service,:annuel,:lieu_stockage, :date_validite);"""
	execute_query(request, query, {'matr':matricule, 'exercice': get_exercice(), 'type':type, 'code':code, 'libelle':libelle,'resp_saisie':resp_saisie,'service':service, 'annuel':annuel, 'lieu_stockage':lieu_stockage, 'date_validite':date_validite})
	
	query = "SELECT max(id) as id FROM dd_justifs WHERE matricule=:matricule AND exercice=:exercice AND type=:type AND code=:code AND service=:service;"
	results = request.dbsession.execute(query, {'matricule':matricule, 'exercice':get_exercice(), 'type':type, 'code': code, 'service': service}).first()
	return results.id		
	
def add_dd_suivi(request, matricule, objet, mail, resp_saisie):
	query = """
	INSERT INTO dd_suivi (matricule, date_envoi, objet, mail, resp_saisie)
	VALUES (:matricule,now(),:objet,:mail,:resp_saisie);"""
	execute_query(request, query, {'matricule':matricule,'objet':objet,'mail':mail,'resp_saisie':resp_saisie})
		
def add_web_ticket_rep(request, matricule, ticket_id, texte):
	query = "INSERT INTO web_tickets_rep(auteur_id, ticket_id, texte) VALUES(:matricule, :ticket_id, :texte);"
	execute_query(request, query, {'matricule': matricule, 'ticket_id': ticket_id, 'texte': texte})
	
	query = "UPDATE web_tickets SET statut=:statut WHERE id=:ticket_id;"
	execute_query(request, query, {'statut':"Fermé", 'ticket_id':ticket_id})	

def attribuer_a(request, id , activite, logged_in, gestionnaire, message):
	if activite=='ayant_droit':
		query = "UPDATE web_ayant_d SET resp_maj=:resp_maj, commentaire=:commentaire, attribue_par=:attribue_par WHERE id=:id;"		
	elif activite=='inscription':
		query = "UPDATE web_inscription SET resp_maj=:resp_maj, commentaire=:commentaire, attribue_par=:attribue_par WHERE id_web=:id;"		
	elif activite=='revenus':
		query = "UPDATE web_revenus SET resp_maj=:resp_maj, commentaire=:commentaire, attribue_par=:attribue_par WHERE matricule=:id and exercice =:exercice;"		
	elif activite=='ticket':
		query = "UPDATE web_tickets SET resp_maj=:resp_maj, commentaire=:commentaire, attribue_par=:attribue_par WHERE id=:id;"
	execute_query(request, query, {'id':id, 'attribue_par': logged_in,'resp_maj':gestionnaire, 'commentaire':message, 'exercice': get_exercice()})
	
def attribuer_ayant_droit(request, id,resp_maj):
	query = "UPDATE web_ayant_d SET resp_maj=:resp_maj WHERE id=:id;"
	execute_query(request, query, {'id':id, 'resp_maj':resp_maj})
	
def attribuer_cesu(request, id,resp_saisie):
	query = "UPDATE web_cde_cesu SET resp_saisie=:resp_saisie WHERE id=:id;"
	execute_query(request, query, {'id':id, 'resp_saisie':resp_saisie})		

def attribuer_chq_vac(request, id,resp_saisie):
	query = "UPDATE web_cde_chq_vac SET resp_saisie=:resp_saisie WHERE id=:id;"
	execute_query(request, query, {'id':id, 'resp_saisie':resp_saisie})	

def attribuer_inscription(request, id,resp_maj):
	query = "UPDATE web_inscription SET resp_maj=:resp_maj WHERE id_web=:id;"
	execute_query(request, query, {'id':id, 'resp_maj':resp_maj})
	
def attribuer_reservation(request, id, resp_maj):
	query = """
UPDATE web_reservation r
INNER JOIN web_reservation_voeu v on v.id_reservation = r.id and v.etat=r.etat
SET r.resp_maj=:resp_maj, v.resp_maj=:resp_maj
WHERE r.id=:id;"""
	execute_query(request, query, {'id':id, 'resp_maj':resp_maj})	
		
def attribuer_revenus(request, id,resp_maj):
	query = "UPDATE web_revenus SET resp_maj=:resp_maj WHERE matricule=:id and exercice=:exercice;"
	execute_query(request, query, {'id':id, 'resp_maj':resp_maj, 'exercice':get_exercice()})	
	
def attribuer_ticket(request, id,resp_maj):
	query = "UPDATE web_tickets SET resp_maj=:resp_maj WHERE id=:id;"
	execute_query(request, query, {'id':id, 'resp_maj':resp_maj})
	
def delete_dd_justifs(request, id):
	query = "DELETE FROM dd_justifs WHERE id=:id"
	execute_query(request, query, {'id': id})
	
def generer_lien():
	mdp = ""

	mini = 15
	maxi = 20
	A = 65
	a = 97
	c = 48

	nbLettres = random.randrange(mini, maxi+1, 1)# randrange(start, stop, [,step]) -> en forme mathématique ça donne [start,stop[

	for i in range(1,nbLettres+1):
		# On va générer un nombre aléatoirement entre 0 et 1
		# Si = 0: Majuscule
		# Si = 1: Minuscule
		carac = random.randrange(0,3,1)
		if carac == 0:
			mdp += chr(random.randrange(A,A+26,1))
		elif carac == 1:
			mdp += chr(random.randrange(c,c+10,1))
		else:
			mdp += chr(random.randrange(a,a+26,1))
	return mdp

def get_categories(request):
	query = "SELECT * FROM p_qualif ORDER BY libelle ASC"
	results = request.dbsession.execute(query)
	return results.fetchall()

def get_cesu_stats(request):
	query = """
SELECT CASE MONTH(c.date_deb)
         WHEN 1 THEN 'janvier'
         WHEN 2 THEN 'fevrier'
         WHEN 3 THEN 'mars'
         WHEN 4 THEN 'avril'
         WHEN 5 THEN 'mai'
         WHEN 6 THEN 'juin'
         WHEN 7 THEN 'juillet'
         WHEN 8 THEN 'aout'
         WHEN 9 THEN 'septembre'
         WHEN 10 THEN 'octobre'
         WHEN 11 THEN 'novembre'
         ELSE 'decembre'  END AS mois,	
(select sum(mtce) from mvt_tes where date_deb <= max(c.date_deb) and millesime=:millesime and stece='CE' and activite='cesu') as cumul,
(select ifnull(sum(w.qte)*15*w.td_tes/100,0) from web_cde_cesu w where etat = 9 and month(c.date_deb)=month(w.datecreate)) as enAttente
from mvt_tes c  where millesime=:millesime and stece='CE' and activite='cesu' group by month(date_deb) order by case when month(date_deb)>8 THEN 0 else 1 end ,month(date_deb);"""
	results = request.dbsession.execute(query, {'millesime': get_millesime()}).fetchall()
	return results	

def get_cesu_report_apayer(request, matricule):
	"""Lire des dossiers de l'adherent group by commission CE """
	query = """
SELECT 
w.*,b.libelle,f.mt_tranche,
w.qte*f.mt_tranche as total,
w.td_tes/100*w.qte*f.mt_tranche as total_ce,
w.qte*f.mt_tranche - w.td_tes/100*w.qte*f.mt_tranche - ifnull((select sum(r.mt_regl) from facture_reg r where r.stece='CE' and r.dossier=w.dossier),0) as total_agent
FROM mvt_consom c 
inner join web_cde_cesu  w on c.dossier=w.dossier and c.matricule = w.matricule
inner join p_bareme b on w.code_bareme = b.code and c.exercice=b.exercice and b.stece='CE'
inner join p_bareme_financier f on w.code_bareme = f.code and c.exercice=f.exercice and f.stece='CE'
WHERE c.stece='CE' AND c.matricule=:matricule AND (c.type_echean = "D")  and c.mtsolde > 0;"""
	results = request.dbsession.execute(query, {'matricule': matricule})
	return results.fetchall()
def get_cesu(request, matricule):
	"""Lire les dossiers CESU du matricule"""
	query = """
SELECT dossier, stece, cion, date_attrib, librubriq, qte1+qte2+qte3+qte4 AS qte, mtfact, mtapayer, dematerialise, mtsolde, millesime FROM mvt_tes 
WHERE stece='CE' AND cion = 'TES' AND matricule=:matricule AND exercice >= YEAR(NOW()) - 5
ORDER BY date_deb DESC;"""
	results = request.dbsession.execute(query, {'matricule': matricule})
	return results.fetchall()
def get_cesu_a_regler(request, matricule):
	"""Lire les dossiers CESU du matricule"""
	query = """
SELECT c.*,b.libelle,f.mt_tranche,
c.qte*f.mt_tranche as total,
td_tes/100*c.qte*f.mt_tranche as total_ce,
c.qte*f.mt_tranche - td_tes/100*c.qte*f.mt_tranche - ifnull((select sum(r.mt_regl) from facture_reg r where r.stece='CE' and r.dossier=c.dossier and r.dossier <> '' and r.dossier is not null),0) as total_agent
FROM web_cde_cesu c
inner join p_bareme b on c.code_bareme = b.code and c.exercice=b.exercice and b.stece='CE'
inner join p_bareme_financier f on c.code_bareme = f.code and c.exercice=f.exercice and f.stece='CE'
WHERE matricule=:matricule AND etat = 9;"""
	results = request.dbsession.execute(query, {'matricule': matricule}).fetchall()
	return results

def get_dossiers(request, stece, cion, matricule):
	"""Lire les dossiers de l'adherent sauf les factures du jour"""
	query = """
SELECT c.date_attrib, c.dossier, c.librubriq, c.mtapayer, c.mtregl, c.mtsolde, c.type_echean, c.type_fact_avoir, f.dossier as facture,
IF(c.date_attrib = CURDATE() and c.type_echean = 'F',False,True) AS link FROM mvt_consom c
left join vac_facture f on f.stece=c.stece and f.dossier=c.dossier
WHERE c.stece=:stece AND c.cion=:cion AND c.matricule=:matricule
ORDER BY date_attrib DESC;"""
	results = request.dbsession.execute(query, {'stece': stece,	'cion': cion,'matricule': matricule})
	return results.fetchall()

def get_factures(request, stece, cion, matricule):
	"""Lire les factures non soldees de l'adherent"""
	query = """
SELECT * FROM mvt_consom
WHERE stece=:stece AND cion=:cion AND matricule=:matricule AND type_echean = "F" AND mtsolde <> 0 AND pointage = 0
ORDER BY date_attrib;"""
	results = request.dbsession.execute(query, {'stece': stece,'cion': cion,'matricule': matricule})
	return results.fetchall()

def get_factures_a_regler(request, matricule):
	query = """
SELECT c.date_attrib,c.stece,c.cion,b.lib_web FROM mvt_consom c LEFT JOIN budget b ON b.stece=c.stece AND b.cion=c.cion	 
WHERE
b.activite='COMMISSION' AND c.matricule=:matricule AND c.mtsolde > 0
AND(
 (c.stece='CE' and b.cion <> 'CHQ_VAC' AND (c.type_echean = "F" or (c.type_echean = "D" and c.cion <>"PRET")))
OR 
 (c.stece in ('SUD','IDF') and c.type_echean = "F"))
GROUP BY c.stece, c.cion,b.lib_web
ORDER BY c.stece, c.cion,b.lib_web;"""
	results = request.dbsession.execute(query, {'matricule': matricule})
	return results.fetchall()

def get_delegation(request):
	query = "SELECT * FROM p_region GROUP BY code ORDER BY code ASC"
	results = request.dbsession.execute(query).fetchall()
	return results

def get_donnant_d(request, matricule):
	query = "SELECT * FROM donnant_d WHERE matricule=:matricule;"
	results = request.dbsession.execute(query, {'matricule': matricule}).first()	
	return results

def get_donnant_d_by_matricule(request, matricule):
	query = "SELECT * FROM donnant_d WHERE matricule=:matricule;"
	results = request.dbsession.execute(query, {'matricule': matricule}).fetchall()
	return results
	
def get_donnant_d_by_name(request, nom):
	query = "SELECT * FROM donnant_d WHERE nom LIKE :nom order by nom,prenom LIMIT 1000;"
	results = request.dbsession.execute(query, {'nom': nom + '%'}).fetchall()
	return results

def get_employeurs_list(request):
	query = "SELECT code FROM p_societe;"
	results = request.dbsession.execute(query).fetchall()
	return results

def get_employeurs(request):
	query = "SELECT code FROM p_societe WHERE web=1 GROUP BY code ORDER BY code DESC;"
	results = request.dbsession.execute(query).fetchall()
	return results

def get_enveloppe(request, code):
	query = "SELECT tranche_sup FROM p_bareme_financier WHERE stece='CE' and exercice=:exercice and code=:code;"
	results = request.dbsession.execute(query,{'exercice': get_exercice(), 'code': code}).first()
	return results

def get_last_connections(request):
	query = "select matricule, nompre, adres1, codpost, ville, dateder_cnx from `donnant_d` ORDER BY dateder_cnx DESC LIMIT 50;"
	results = request.dbsession.execute(query)
	return results

def get_cheque_vacances(request, param1):
	"""Lire le(s) dossier(s) cheque-vacances de l'adherent :
	 - param1 = 6 carac. : par le matricule
	 - param1 > 6 caract : par le no de dossier"""
	query = """
SELECT m.*, r.bic, r.iban, r.titulaire FROM mvt_chq_vac m 
INNER JOIN dd_rib r ON m.idrib=r.idrib
WHERE m.stece='CE' AND m.cion = 'CHQ_VAC'"""
	if len(param1) == 5:
		query += """ AND m.matricule=:param1 ORDER BY m.DATE_DEB DESC;"""
	else:
		query += """ AND m.dossier=:param1;"""

	results = request.dbsession.execute(query, {'param1': param1})
	return results.fetchall()

def get_last_payments(request):
	query = """
SELECT 
	DATE_FORMAT(t.dateoperation,'%d/%m/%Y') as dateoperation, DATE_FORMAT(t.date_create,'%d/%m/%Y - %H:%i') as datecreate,
	t.listedossier, t.mtoperation, DATE_FORMAT(t.datepremierprelev,'%d/%m/%Y') as datepremierprelev, 
	DATE_FORMAT(t.dateprochainprelev,'%d/%m/%Y') as dateprochainprelev, t.mtprochainprelev,
	IF (traite = 1, 'OUI', 'NON') as traite,
	IF (t.msgerreur > 100 and t.msgerreur < 200, (Select c.libelle from p_paybox_reponse_centre_autorisation c where c.code = substr(t.msgerreur,4)),p.libelle) as libelle,
	IF (m.matricule, m.matricule, c.matricule) as matricule
	FROM t_ticketcb t 
	left join p_paybox_reponse p on p.code=t.msgerreur
    LEFT JOIN mvt_consom m ON m.stece=t.stece and m.dossier=SUBSTRING_INDEX(t.listedossier,'/',1)
    LEFT JOIN web_cde_cesu c ON c.id=SUBSTRING_INDEX(t.listedossier,'/',1)
	ORDER BY date_create DESC 
    LIMIT 5000;"""
	results = request.dbsession.execute(query, {'exercice': get_exercice()})
	return results.fetchall()

def get_member_regions(request, matricule):
	"""Lire les dossiers de l'adherent group by REGION"""
	query = """
SELECT b.stece,b.cion,b.lib_web FROM mvt_consom c LEFT JOIN budget b ON b.stece=c.stece AND b.cion=c.cion	 
WHERE c.stece<>'CE' AND b.activite='COMMISSION' AND c.matricule=:matricule
GROUP BY c.stece, c.cion,b.lib_web
ORDER BY c.cion;"""
	results = request.dbsession.execute(query, {'matricule': matricule})
	return results.fetchall()

def get_member_commissions(request, stece, matricule):
	"""Lire des dossiers de l'adherent group by commission CE """
	query = """
SELECT g.stece, g.cion,g.lib_web from
(SELECT b.stece, b.cion,b.lib_web FROM mvt_consom c LEFT JOIN budget b ON b.stece=c.stece AND b.cion=c.cion 
WHERE c.stece=:stece AND b.activite='COMMISSION' AND c.matricule=:matricule GROUP BY b.stece, b.cion,b.lib_web
UNION ALL 
SELECT b.stece, b.cion, b.lib_web FROM mvt_enfance c LEFT JOIN budget	b ON b.stece=c.stece AND b.cion=c.cion 
WHERE c.stece=:stece AND b.activite='COMMISSION' AND c.matricule=:matricule GROUP BY b.stece, b.cion,b.lib_web)
AS g
ORDER BY g.lib_web;"""
	results = request.dbsession.execute(query, {'stece': stece, 'matricule': matricule})
	return results.fetchall()

def get_marge_cesu(request):
	query = """
SELECT CASE MONTH(datecreate)
         WHEN 1 THEN 'janvier'
         WHEN 2 THEN 'fevrier'
         WHEN 3 THEN 'mars'
         WHEN 4 THEN 'avril'
         WHEN 5 THEN 'mai'
         WHEN 6 THEN 'juin'
         WHEN 7 THEN 'juillet'
         WHEN 8 THEN 'aout'
         WHEN 9 THEN 'septembre'
         WHEN 10 THEN 'octobre'
         WHEN 11 THEN 'novembre'
         ELSE 'decembre'  END AS mois,	
    COUNT(
		CASE 
            WHEN millesime = :millesimem1 THEN 1 ELSE NULL 
        END
	) as 'exercicem1',
    COUNT(
		CASE 
            WHEN millesime = :millesime THEN 1 ELSE NULL 
        END
	) as 'exercice'
FROM web_cde_cesu where millesime in (:millesime,:millesimem1) group by month(datecreate)  
order by case when month(datecreate) > 8 then 0 else 1 end, month(datecreate);
"""
	results = request.dbsession.execute(query, {'millesime': get_millesime(), 'millesimem1': get_millesime() - 1}).fetchall()
	return results	

def get_member_info(request, userid):
	query = "select resp_saisie, nom, prenom from p_user where resp_saisie=:userid"
	result = request.dbsession.execute(query, {'userid': userid}).first()
	return {'fullname': result[0],'email': result[1]}

def get_nb_web_ayant_d_by_etat(request, etat):
	query = "SELECT COUNT(*) FROM web_ayant_d WHERE etat=:etat and exercice=:exercice;"
	results = request.dbsession.execute(query, {'etat': etat, 'exercice': get_exercice()}).first()
	return results[0]	
		
def get_nb_web_cde_cesu_by_etat(request, etat):
	query = "SELECT COUNT(*) FROM web_cde_cesu WHERE etat=:etat and millesime=:millesime;"
	results = request.dbsession.execute(query, {'etat': etat, 'millesime': get_millesime()}).first()
	return results[0]		
	
def get_nb_web_cde_chq_vac_by_etat(request, etat):
	query = "SELECT COUNT(*) FROM web_cde_chq_vac WHERE etat=:etat and exercice=:exercice;"
	results = request.dbsession.execute(query, {'etat': etat, 'exercice': get_exercice()}).first()
	return results[0]		

def get_nb_web_inscription_by_etat(request, etat):
	query = "SELECT COUNT(*) FROM web_inscription WHERE etat=:etat and exercice=:exercice;"
	results = request.dbsession.execute(query, {'etat': etat, 'exercice': get_exercice()}).first()
	return results[0]	

def get_nb_web_resa_by_etat(request, etat):
	query = "SELECT COUNT(*) FROM web_reservation WHERE etat=:etat and exercice=:exercice;"
	results = request.dbsession.execute(query, {'etat': etat, 'exercice': get_exercice()}).first()
	return results[0]	
	
	
def get_nb_web_revenus_by_etat(request, etat):
	query = "SELECT COUNT(*) FROM web_revenus WHERE etat=:etat and exercice=:exercice;"
	results = request.dbsession.execute(query, {'etat': etat, 'exercice': get_exercice()}).first()
	return results[0]
		
def get_nb_web_tickets_by_statut(request, statut):
	query = "SELECT COUNT(*) FROM web_tickets WHERE statut=:statut;"
	results = request.dbsession.execute(query, {'statut': statut}).first()
	return results[0]

def get_dd_justifs_by_type_exercice(request, matricule, type, code, exercice):
	query = """
SELECT * FROM dd_justifs 
WHERE matricule=:matricule AND type=:type AND code=:code AND exercice=:exercice and lieu_stockage <> "";""" 
	results = request.dbsession.execute(query, {'matricule': matricule, 'exercice': exercice, 'type': type, 'code': code}).first()
	return results

def get_ddjustifs(request, matricule, type, code):
	if "ALL" in code:
		query = """
SELECT d.* FROM dd_justifs d
INNER JOIN p_justifs p on p.type=d.type and p.code=d.code
WHERE d.matricule=:matricule AND d.service='WEB' AND d.lieu_stockage<>'NON'
 AND (d.exercice=:exercice  or (p.annuel <> 0 and NOW()<d.date_validite))  order by p.libelle ;"""
		results = request.dbsession.execute(query, {'matricule': matricule, 'exercice': get_exercice()}).fetchall()
	else:
		query = """
SELECT d.* FROM dd_justifs d
INNER JOIN p_justifs p on p.type=d.type and p.code=d.code
WHERE matricule=:matricule AND d.service='WEB' AND d.lieu_stockage<>'NON'
AND ((d.exercice=:exercice AND d.type=:type AND d.code=:code) or (p.annuel <> 0 and NOW()<d.date_validite));"""
		results = request.dbsession.execute(query, {'matricule': matricule, 'exercice': get_exercice(), 'type': type, 'code': code}).fetchall()

	return results

def get_dd_justifs_by_exercice(request, matricule, exercice):
	query = """
SELECT * FROM dd_justifs 
WHERE matricule=:matricule AND service='WEB' AND lieu_stockage<>'NON'
 AND (exercice=:exercice  and annuel = 0) ;"""
	results = request.dbsession.execute(query, {'matricule': matricule, 'exercice': exercice}).fetchall()
	return results

def get_ddjustifs_by_id(request, id):
	query = "SELECT *, DATE_FORMAT(date_validite, '%d/%m/%Y') as date_validite_convertie FROM dd_justifs WHERE id=:id;"
	results = request.dbsession.execute(query, {'id': id}).first()
	return results

def get_p_justifs_web(request):
	query = """
SELECT p.*
FROM p_justifs p
WHERE p.WEB > 0 
ORDER BY p.libelle;
		"""
	results = request.dbsession.execute(query).fetchall()
	return results

def get_parametre(request, code):
	query = """select b.debut_v1, b.fin_v1, b.debut_v2,b.fin_v2, b.date_millesime, f.tranche_sup, f.mt_plafond
	from p_bareme b inner join p_bareme_financier f on b.stece=f.stece and b.exercice = f.exercice and b.code = f.code
	WHERE b.stece='CE' and b.exercice=:exercice and b.code=:code;"""
	results = request.dbsession.execute(query, {'exercice':get_exercice(), 'code': code}).first()
	return results

def get_parametre_by_exercice(request, exercice, code):
	query = """select b.debut_v1, b.fin_v1, b.debut_v2,b.fin_v2, b.date_millesime, f.tranche_sup, f.mt_plafond
	from p_bareme b inner join p_bareme_financier f on b.stece=f.stece and b.exercice = f.exercice and b.code = f.code
	WHERE b.stece='CE' and b.exercice=:exercice and b.code=:code;"""
	results = request.dbsession.execute(query, {'exercice':exercice, 'code': code}).first()
	return results

def get_p_justif(request, type, code):
	query = "SELECT *  FROM p_justifs WHERE type = :type AND code = :code;"
	results = request.dbsession.execute(query, { 'type':type, 'code':code}).first()
	return results	
	
def get_p_justif_by_libelle(request, libelle):
	query = "select * from p_justifs where libelle =:libelle;"
	results = request.dbsession.execute(query, { 'libelle':libelle}).first()
	return results	

def get_p_justif_by_activite(request, activite):
	query = "SELECT * FROM p_justifs WHERE USED_IN like :used_in order by libelle;"
	results = request.dbsession.execute(query, { 'used_in':'%'+activite+'%'}).fetchall()
	return results	

def get_p_user(request, logged_in):
	query = "select nom,prenom,mel,niveau_web from p_user where resp_saisie=:logged_in;"
	results = request.dbsession.execute(query, {'logged_in': logged_in}).first()
	return results

def get_reglements(request, matricule):
	query = "SELECT date_attrib,cion,activite,mode_regl,mtregl FROM mvt_consom WHERE matricule=:matricule and type_echean ='R' order by date_attrib DESC LIMIT 15;"
	results = request.dbsession.execute(query, {'matricule': matricule}).fetchall()
	return results

def get_subventions(request, matricule):
	query = "SELECT date_attrib, dossier, librubriq ,mtfact FROM mvt_enfance WHERE matricule=:matricule order by date_attrib DESC LIMIT 15;"
	results = request.dbsession.execute(query, {'matricule': matricule}).fetchall()
	return results

def	get_rib_by_id(request, id_rib):
	query = "SELECT * FROM dd_rib WHERE idrib=:id_rib;"
	results = request.dbsession.execute(query, {'id_rib': id_rib}).first()
	return results
	
def get_situation_familiale(request):
	query = "SELECT code, libelle FROM p_situfam WHERE code<>'NR'GROUP BY code ORDER BY code ASC"
	results = request.dbsession.execute(query)
	return results.fetchall()

def get_situation_familiale_by_code(request, code):
	query = "SELECT code, libelle FROM p_situfam WHERE code=:code"
	results = request.dbsession.execute(query, {'code': code}).first()
	return results

def get_web_cheques_stats(request):
		"""Lire les stats sur web_cde_chq_vac"""
		query = """
select DAY(datecreate) AS jour,
    COUNT(
        CASE 
            WHEN etat = 11 THEN 1 ELSE NULL 
        END
    ) AS 'invalide',
    COUNT(
        CASE 
            WHEN etat = 10 THEN 1 ELSE NULL 
        END
    ) AS 'valide',
    COUNT(
        CASE 
            WHEN etat = 2 THEN 1 ELSE NULL 
        END
    ) AS 'incomplet',
    COUNT(
        CASE 
            WHEN etat = 1 THEN 1 ELSE NULL 
        END
    ) AS 'atraiter'
from web_cde_chq_vac where datecreate >= DATE_ADD(now(), INTERVAL -15 DAY) and etat > 0 group by DATE(datecreate);
"""
		results = request.dbsession.execute(query).fetchall()
		return results

def get_web_inscrits_stats(request):
		"""Lire les stats sur web_revenus"""
		query = """
select DAY(datecreat) AS jour,
    COUNT(
        CASE 
            WHEN etat = 11 THEN 1 ELSE NULL 
        END
    ) AS 'invalide',
    COUNT(
        CASE 
            WHEN etat = 10 THEN 1 ELSE NULL 
        END
    ) AS 'valide',
    COUNT(
        CASE 
            WHEN etat = 2 THEN 1 ELSE NULL 
        END
    ) AS 'incomplet',
    COUNT(
        CASE 
            WHEN etat = 1 THEN 1 ELSE NULL 
        END
    ) AS 'atraiter'
from web_inscription where datecreat >= DATE_ADD(now(), INTERVAL -15 DAY) and etat > 0 group by DATE(datecreat);
"""
		results = request.dbsession.execute(query).fetchall()
		return results

def get_web_revenus_stats(request):
		"""Lire les stats sur web_revenus"""
		query = """
select DAY(datecreat) AS jour,
    COUNT(
        CASE 
            WHEN etat = 11 THEN 1 ELSE NULL 
        END
    ) AS 'invalide',
    COUNT(
        CASE 
            WHEN etat = 10 THEN 1 ELSE NULL 
        END
    ) AS 'valide',
    COUNT(
        CASE 
            WHEN etat = 2 THEN 1 ELSE NULL 
        END
    ) AS 'incomplet',
    COUNT(
        CASE 
            WHEN etat = 1 THEN 1 ELSE NULL 
        END
    ) AS 'atraiter'
from web_revenus where datecreat >= DATE_ADD(now(), INTERVAL -15 DAY) and etat > 0 group by DATE(datecreat);
"""
		results = request.dbsession.execute(query).fetchall()
		return results
	
def get_web_reponses(request, typerep, rubrique="inscription"):
	query = "SELECT texte as libelle FROM web_reponses WHERE type=:typerep AND (rubrique=:rub OR rubrique='ALL') GROUP BY texte ASC;"
	
	results = request.dbsession.execute(query, {'typerep': typerep, 'rub': rubrique}).fetchall()
	return results

def get_cesu_realise(request):
	query = """
SELECT millesime, sum(mtce) as mtce, sum(mtfact) as mtfact, count(*) as nombre_dossiers, 
(select count(distinct(matricule)) from mvt_tes WHERE stece='CE' and cion='TES' and activite='CESU'  and millesime=m.millesime) as nombre_agents
FROM ganaelbd.mvt_tes m
WHERE stece='CE' and cion='TES' and activite='CESU' and millesime IN (:millesime,:millesimem1)
group by millesime order by millesime desc;
"""
	results = request.dbsession.execute(query, {'millesime': get_millesime(), 'millesimem1': get_millesime() - 1}).fetchall()
	return results
def get_cesu_realise_envoyer(request):
	query = """
SELECT millesime, sum(mtce) as realise, sum(mtfact) as mtfact, count(*) as nombre,
(select round(ifnull(sum(qte)*15*td_tes/100,0),2) from web_cde_cesu where etat < 10 and millesime =m.millesime) as previsionnelle,
(SELECT count(*) from web_cde_cesu w  where w.etat=10 and w.millesime=m.millesime) as web,
(SELECT count(*) from mvt_tes where dematerialise=0 and stece='CE' and cion='TES' and activite='CESU' and date_envoi_chq is not null and millesime=m.millesime) as materialise,
(SELECT count(*) from mvt_tes where dematerialise=1 and stece='CE' and cion='TES' and activite='CESU' and date_envoi_chq is not null and millesime=m.millesime) as dematerialise
FROM ganaelbd.mvt_tes m
WHERE stece='CE' and cion='TES' and activite='CESU' and date_envoi_chq is not null and millesime IN (:millesime,:millesimem1)
group by millesime order by millesime desc;
"""
	results = request.dbsession.execute(query, {'millesime': get_millesime(), 'millesimem1': get_millesime() - 1}).fetchall()
	return results

def get_cesu_previsionnelle(request):
	query = """
SELECT sum(m.mtce) as montant_web,
    COUNT(
        CASE 
            WHEN w.etat in (1,2)THEN 1 ELSE NULL 
        END
    ) AS 'non_traite',
    COUNT(
        CASE 
            WHEN w.etat = 9 THEN 1 ELSE NULL 
        END
    ) AS 'a_regler',
    COUNT(
        CASE 
            WHEN w.etat = 10  and m.date_envoi_chq is null THEN 1 ELSE NULL 
        END
    ) AS 'regler',
    (SELECT sum(mtce) from mvt_tes where millesime =2018 and date_envoi_chq is null) as montant_total
    from web_cde_cesu w 
    left join mvt_tes m on m.stece='ce' and m.dossier=w.dossier and m.date_envoi_chq is null
    where w.millesime = :millesime"""
	results = request.dbsession.execute(query, {'millesime': get_millesime()}).fetchall()
	return results

def get_web_cesu_stats(request):
	query = """
SELECT sum(mtce)
(SELECT count(*) from web_cde_cesu w  where etat in 1,2) as Non traité,
(SELECT count(*) from web_cde_cesu w  where etat = 9) as A régler,
(SELECT count(*) from web_cde_cesu w where w.dossier=m.dossier where etat = 10) as Réglé,
(select round(ifnull(sum(qte)*15*td_tes/100,0),2) from web_cde_cesu where millesime =m.millesime and m.date_envoi_chq is null) as previsionnelle_web,
FROM ganaelbd.mvt_tes m
WHERE stece='CE' and cion='TES' and activite='CESU' and date_envoi_chq is not null and millesime IN (:millesime,:millesimem1)
group by millesime order by millesime desc;
"""
	results = request.dbsession.execute(query, {'millesime': get_millesime(), 'millesimem1': get_millesime() - 1}).fetchall()
	return results

def get_cesu_realise_by_millesime(request):
	"""Lire les stats sur mvt_conso"""
	millesime = get_millesime()
	millesimem1 = get_millesime() - 1
	query = """
SELECT millesime, sum(mtce) as realise, sum(mtfact) as mtfact, count(*) as nombre,
(select round(ifnull(sum(qte)*15*td_tes/100,0),2) from web_cde_cesu where etat < 10 and millesime =m.millesime) as previsionnelle,
(SELECT count(*) from web_cde_cesu where etat=10 and millesime=m.millesime) as web,
(SELECT count(*) from mvt_tes where dematerialise=0 and stece='CE' and cion='TES' and activite='CESU' and date_envoi_chq is not null and millesime=m.millesime) as materialise,
(SELECT count(*) from mvt_tes where dematerialise=1 and stece='CE' and cion='TES' and activite='CESU' and date_envoi_chq is not null and millesime=m.millesime) as dematerialise
FROM ganaelbd.mvt_tes m
WHERE stece='CE' and cion='TES' and activite='CESU' and date_envoi_chq is not null and millesime =:millesime
group by millesime order by millesime desc;
"""
	results = request.dbsession.execute(query, {'millesime': get_millesime()}).first()
	return results
def get_cheques_realise(request):
	"""Lire les stats sur mvt_conso"""
	query = """
SELECT exercice, IFNULL(sum(mtce),0) as realise, sum(mtfact) as mtfact, count(*) as nombre,
(select IFNULL(sum(mtce),0) from web_cde_chq_vac where etat < 10 and exercice = m.exercice) as previsionnelle,
(SELECT count(*) from web_cde_chq_vac where etat=10 and exercice=m.exercice) as web
FROM ganaelbd.mvt_chq_vac m
WHERE stece='CE' and cion='CHQ_VAC' and activite='CHQ_VAC' and exercice>'2015'
group by exercice order by exercice desc;
"""
	results = request.dbsession.execute(query, ).fetchall()
	return results

def get_cheques_realise_by_exercice(request, exercice):
	"""Lire les stats sur mvt_conso"""
	query = """
SELECT exercice, IFNULL(sum(mtce),0) as realise, sum(mtfact) as mtfact, count(*) as nombre,
(select IFNULL(sum(mtce),0) from web_cde_chq_vac where etat < 10 and exercice = m.exercice) as previsionnelle
FROM ganaelbd.mvt_chq_vac m
WHERE stece='CE' and cion='CHQ_VAC' and activite='CHQ_VAC' and exercice=:exercice
group by exercice order by exercice desc;
"""
	results = request.dbsession.execute(query, {'exercice': exercice}).first()
	return results

def get_decision_cesu(request, cas):
	if cas == 'accepte':
		query = """
SELECT d.matricule,d.mail,t.qte1,t.qte_souhaitee, f.mt_plafond,f.libelle,c.qf FROM mvt_tes t
inner join p_bareme_financier f on f.libelle = t.librubriq and f.exercice=t.exercice
inner join donnant_d d on t.matricule=d.matricule
inner join carte_famil_d c on c.matricule=t.matricule and c.exercice=t.exercice
where t.exercice =:exercice and t.stece=:stece and t.qte1 < t.qte_souhaitee and t.num_vague=1 and c.qf<=f.mt_plafond
"""
	elif cas == 'refuse':
		query = """
SELECT d.matricule,d.mail,t.qte1,t.qte_souhaitee, f.mt_plafond,f.libelle,c.qf FROM mvt_tes t
inner join p_bareme_financier f on f.libelle = t.librubriq and f.exercice=t.exercice
inner join donnant_d d on t.matricule=d.matricule
inner join carte_famil_d c on c.matricule=t.matricule and c.exercice=t.exercice
where t.exercice =:exercice and t.stece=:stece and t.qte1 < t.qte_souhaitee and t.num_vague=1 and c.qf>f.mt_plafond
"""
	results = request.dbsession.execute(query, {'exercice':get_exercice(),'stece':'ce'}).fetchall()
	return results

def get_web_inscrits_stats(request):
	"""Lire les stats sur web_revenus"""
	query = """
select DAY(datecreat) AS jour,
    COUNT(
        CASE 
            WHEN etat = 11 THEN 1 ELSE NULL 
        END
    ) AS 'invalide',
    COUNT(
        CASE 
            WHEN etat = 10 THEN 1 ELSE NULL 
        END
    ) AS 'valide',
    COUNT(
        CASE 
            WHEN etat = 2 THEN 1 ELSE NULL 
        END
    ) AS 'incomplet',
    COUNT(
        CASE 
            WHEN etat = 1 THEN 1 ELSE NULL 
        END
    ) AS 'atraiter'
from web_inscription where datecreat >= DATE_ADD(now(), INTERVAL -15 DAY) and etat > 0 group by DATE(datecreat);
"""
	results = request.dbsession.execute(query).fetchall()
	return results

def get_web_revenus_stats(request):
	"""Lire les stats sur web_revenus"""
	query = """
select DAY(datecreat) AS jour,
    COUNT(
        CASE 
            WHEN etat = 11 THEN 1 ELSE NULL 
        END
    ) AS 'invalide',
    COUNT(
        CASE 
            WHEN etat = 10 THEN 1 ELSE NULL 
        END
    ) AS 'valide',
    COUNT(
        CASE 
            WHEN etat = 2 THEN 1 ELSE NULL 
        END
    ) AS 'incomplet',
    COUNT(
        CASE 
            WHEN etat = 1 THEN 1 ELSE NULL 
        END
    ) AS 'atraiter'
from web_revenus where datecreat >= DATE_ADD(now(), INTERVAL -15 DAY) and etat > 0 group by DATE(datecreat);
"""
	results = request.dbsession.execute(query).fetchall()
	return results

def get_stats_web_inscription(request):
		"""Lire les stats sur web_revenus"""
		query = """
SELECT CASE MONTH(datecreat)
         WHEN 1 THEN 'janvier'
         WHEN 2 THEN 'fevrier'
         WHEN 3 THEN 'mars'
         WHEN 4 THEN 'avril'
         WHEN 5 THEN 'mai'
         WHEN 6 THEN 'juin'
         WHEN 7 THEN 'juillet'
         WHEN 8 THEN 'aout'
         WHEN 9 THEN 'septembre'
         WHEN 10 THEN 'octobre'
         WHEN 11 THEN 'novembre'
         ELSE 'decembre'  END AS mois,	
    COUNT(
		CASE 
            WHEN exercice = :exercicem1 THEN 1 ELSE NULL 
        END
	) as 'exercicem1',
    COUNT(
		CASE 
            WHEN exercice = :exercice THEN 1 ELSE NULL 
        END
	) as 'exercice'
FROM web_inscription where exercice in (:exercice,:exercicem1) group by month(datecreat)  
order by case when  month(datecreat) > 8 then 0 else 1 end, month(datecreat);
"""
		results = request.dbsession.execute(query, {'exercice': get_exercice(), 'exercicem1': get_exercice() - 1}).fetchall()
		return results

def get_stats_web_ayant_d(request):
		"""Lire les stats sur ayant_d"""
		query = """
SELECT CASE MONTH(date_creat)
         WHEN 1 THEN 'janvier'
         WHEN 2 THEN 'fevrier'
         WHEN 3 THEN 'mars'
         WHEN 4 THEN 'avril'
         WHEN 5 THEN 'mai'
         WHEN 6 THEN 'juin'
         WHEN 7 THEN 'juillet'
         WHEN 8 THEN 'aout'
         WHEN 9 THEN 'septembre'
         WHEN 10 THEN 'octobre'
         WHEN 11 THEN 'novembre'
         ELSE 'decembre'  END AS mois,	
    COUNT(
		CASE 
            WHEN exercice = :exercicem1 THEN 1 ELSE NULL 
        END
	) as 'exercicem1',
    COUNT(
		CASE 
            WHEN exercice = :exercice THEN 1 ELSE NULL 
        END
	) as 'exercice'
FROM web_ayant_d where exercice in (:exercice,:exercicem1) group by month(date_creat)  
order by case when  month(date_creat) > 8 then 0 else 1 end, month(date_creat);
"""
		results = request.dbsession.execute(query, {'exercice': get_exercice(), 'exercicem1': get_exercice() - 1}).fetchall()
		return results	
	
def get_stats_web_revenus(request):
		"""Lire les stats sur web_revenus"""
		query = """
SELECT CASE MONTH(datecreat)
         WHEN 1 THEN 'janvier'
         WHEN 2 THEN 'fevrier'
         WHEN 3 THEN 'mars'
         WHEN 4 THEN 'avril'
         WHEN 5 THEN 'mai'
         WHEN 6 THEN 'juin'
         WHEN 7 THEN 'juillet'
         WHEN 8 THEN 'aout'
         WHEN 9 THEN 'septembre'
         WHEN 10 THEN 'octobre'
         WHEN 11 THEN 'novembre'
         ELSE 'decembre'  END AS mois,		
    COUNT(
		CASE 
            WHEN exercice = :exercicem1 THEN 1 ELSE NULL 
        END
	) as 'exercicem1',
    COUNT(
		CASE 
            WHEN exercice = :exercice THEN 1 ELSE NULL 
        END
	) as 'exercice'
FROM web_revenus where exercice in (:exercice,:exercicem1) group by month(datecreat)  
order by case when  month(datecreat) > 8 then 0 else 1 end, month(datecreat);
"""
		results = request.dbsession.execute(query, {'exercice': get_exercice(), 'exercicem1': get_exercice() - 1}).fetchall()
		return results
	
def get_stats_cesu(request):
	"""Lire les stats sur web_tickets"""
	query = """
SELECT CASE MONTH(datecreate)
         WHEN 1 THEN 'janvier'
         WHEN 2 THEN 'fevrier'
         WHEN 3 THEN 'mars'
         WHEN 4 THEN 'avril'
         WHEN 5 THEN 'mai'
         WHEN 6 THEN 'juin'
         WHEN 7 THEN 'juillet'
         WHEN 8 THEN 'aout'
         WHEN 9 THEN 'septembre'
         WHEN 10 THEN 'octobre'
         WHEN 11 THEN 'novembre'
         ELSE 'decembre'  END AS mois,	
    COUNT(
		CASE 
            WHEN exercice = :exercicem1 THEN 1 ELSE NULL 
        END
	) as 'exercicem1',
    COUNT(
		CASE 
            WHEN exercice = :exercice THEN 1 ELSE NULL 
        END
	) as 'exercice'
FROM web_cde_cesu where exercice in (:exercice,:exercicem1) group by month(datecreate)  
order by case when month(datecreate) > 8 then 0 else 1 end, month(datecreate);
"""
	results = request.dbsession.execute(query, {'exercice': get_exercice(), 'exercicem1': get_exercice() - 1}).fetchall()
	return results	

def get_stats_chq_vac(request):
		"""Lire les stats sur web_cde_chq_vac"""
		query = """
SELECT CASE MONTH(datecreate)
         WHEN 1 THEN 'janvier'
         WHEN 2 THEN 'fevrier'
         WHEN 3 THEN 'mars'
         WHEN 4 THEN 'avril'
         WHEN 5 THEN 'mai'
         WHEN 6 THEN 'juin'
         WHEN 7 THEN 'juillet'
         WHEN 8 THEN 'aout'
         WHEN 9 THEN 'septembre'
         WHEN 10 THEN 'octobre'
         WHEN 11 THEN 'novembre'
         ELSE 'decembre'  END AS mois,	
    COUNT(
		CASE 
            WHEN exercice = :exercicem1 THEN 1 ELSE NULL 
        END
	) as 'exercicem1',
    COUNT(
		CASE 
            WHEN exercice = :exercice THEN 1 ELSE NULL 
        END
	) as 'exercice'
FROM web_cde_chq_vac where exercice in (:exercice,:exercicem1) group by month(datecreate)  
order by case when  month(datecreate) > 8 then 0 else 1 end, month(datecreate);
"""
		results = request.dbsession.execute(query, {'exercice': get_exercice(), 'exercicem1': get_exercice() - 1}).fetchall()
		return results
	
def get_stats_gestionnaires(request):
	query = """
	select u.resp_saisie, r.libelle as region,
	(select count(*) from web_ayant_d a where a.resp_maj=u.resp_saisie and exercice=:exercice and etat=10) as nb_ayant_droit,
	(select count(*) from web_inscription i where i.resp_maj=u.resp_saisie and exercice=:exercice and etat=10) as nb_inscription,
	(select count(*) from web_revenus r where r.resp_maj=u.resp_saisie and exercice=:exercice and etat=10) as nb_declaration_revenus,
	(select count(*) from web_tickets t where t.resp_maj=u.resp_saisie and exercice=:exercice and statut='ferme') as nb_tickets,
	(select count(*) from web_cde_cesu ce where ce.resp_saisie=u.resp_saisie and exercice=:exercice and etat=10) as nb_cesu,
	(select count(*) from web_cde_chq_vac cv where cv.resp_saisie=u.resp_saisie and exercice=:exercice and etat=10) as nb_cv
	from p_user u
	inner join donnant_d d on d.matricule = u.matricule
	inner join p_region r on r.code = d.cod_region
	where u.niveau_web is not null"""
	results = request.dbsession.execute(query, {'exercice': get_exercice()}).fetchall()
	return results	
	
def get_stats_tickets(request):
		"""Lire les stats sur web_tickets"""
		query = """
SELECT CASE MONTH(cree_le)
         WHEN 1 THEN 'janvier'
         WHEN 2 THEN 'fevrier'
         WHEN 3 THEN 'mars'
         WHEN 4 THEN 'avril'
         WHEN 5 THEN 'mai'
         WHEN 6 THEN 'juin'
         WHEN 7 THEN 'juillet'
         WHEN 8 THEN 'aout'
         WHEN 9 THEN 'septembre'
         WHEN 10 THEN 'octobre'
         WHEN 11 THEN 'novembre'
         ELSE 'decembre'  END AS mois,	
    COUNT(
		CASE 
            WHEN exercice = :exercicem1 THEN 1 ELSE NULL 
        END
	) as 'exercicem1',
    COUNT(
		CASE 
            WHEN exercice = :exercice THEN 1 ELSE NULL 
        END
	) as 'exercice'
FROM web_tickets where exercice in (:exercice,:exercicem1) group by month(cree_le)  
order by case when month(cree_le) > 8 then 0 else 1 end, month(cree_le);
"""
		results = request.dbsession.execute(query, {'exercice': get_exercice(), 'exercicem1': get_exercice() - 1}).fetchall()
		return results

def get_suivi_mail(request, cas):
	query = """SELECT d.mail,c.qte,c.qte_souhaite FROM web_cde_cesu c
inner join donnant_d d on d.matricule=c.matricule
where c.qte_souhaite>c.qte and c.code_bareme not in (00050305,00050306) and c.etat =:cas and exercice=:exercice;
"""
	results = request.dbsession.execute(query, {'cas': cas, 'exercice': get_exercice()}).fetchall()
	return results
