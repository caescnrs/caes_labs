# -*- coding: utf8 -*-
from pyramid_layout.panel import panel_config
from pyramid.security import authenticated_userid

from .models.default import *

@panel_config(name='dropdown_menu_panel', renderer='templates/panels/dropdown_menu_panel.pt')
def dropdown_menu_panel(context, request):
    logged_in = authenticated_userid(request)
    if logged_in is None:
        return { 'logged_in': '' }
    else:
        user = get_p_user(request, logged_in)
    return {
        'logged_in': logged_in,
        'user': user,
    }

@panel_config(name='ayant_droit_panel', renderer='templates/panels/ayant_droit_panel.pt')
def ayant_droit_panel(context, request):
    matricule = context
    return {
        'ayant_droits': get_ayant_droits(request, matricule),
        'web_ayant_droits': get_web_ayant_d_by_matricule(request, matricule),
        'abc': "bg-success",
    }