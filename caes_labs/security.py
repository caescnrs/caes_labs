from .models.default import get_group
def groupfinder(login, request):
	gestionnaire = get_group(request, login)
	if gestionnaire:
		if gestionnaire.niveau_web == u'SAC':
			return ['group:sac']
		elif gestionnaire.niveau_web == u'SU':
			return ['group:su']
		elif gestionnaire.niveau_web == u'ADMIN':
			return ['group:administrators']
		elif gestionnaire.niveau_web == u'GESTION':
			return ['group:gestionnaires']	
	else:
		return []  # it means that login is logged in (it returns None if login isn't logged in)
