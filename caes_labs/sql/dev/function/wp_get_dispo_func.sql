CREATE DEFINER=`sa`@`` FUNCTION `wp_get_dispo_func`(
    pid_catalogue int(11),
    pid_type_destination int(11),
    pid_destination int(11),
    pid_hebergement int(11),
    pdate date   
    ) RETURNS int(11)
BEGIN
    declare qte_dispo int(11) default 0;
    declare qte_reserve int(11) default 0;
    declare pourcent int(11) default 0;    
    
    select sum(c.qte) into qte_dispo from `vac_association_cata2` c inner join `vac_periodes2` p
    on p.id = c.id_periode
    where c.id_catalogue = pid_catalogue and c.id_type_destination = pid_type_destination
    and c.id_destination = pid_destination and c.id_hebergement = pid_hebergement and
    pdate between p.date_debut and p.date_fin;
    
    RETURN qte_dispo;
END