CREATE DEFINER=`sa`@`localhost` FUNCTION `wp_get_pourcent_func`(
    pid_catalogue int(11),
    pid_type_destination int(11),
    pid_destination int(11),
    pid_hebergement int(11),
    pdate date  
    ) RETURNS int(11)
BEGIN
    declare qte_dispo int(11) default 0;
    declare qte_reserve int(11) default 0;
    declare pourcent int(11) default 0;
        
    select sum(c.qte) into qte_dispo from `vac_association_cata2` c inner join `vac_periodes2` p
    on p.id = c.id_periode
    where c.id_catalogue = pid_catalogue and c.id_type_destination = pid_type_destination
    and c.id_destination = pid_destination and c.id_hebergement = pid_hebergement and
    pdate between p.date_debut and p.date_fin;
    
    select sum(c.qte) into qte_reserve from `vac_resa2` r inner join `vac_resa_conf2` c
    on c.stece = r.stece and c.dossier = r.dossier
    where r.id_catalogue = pid_catalogue and r.id_type_destination = pid_type_destination
    and r.id_destination = pid_destination and c.id_hebergement = pid_hebergement
    and pdate between r.date_deb_resa and r.date_fin_resa;  
    
    if isnull(qte_dispo) then
        set qte_dispo= 0;
    end if;
    
RETURN (1-(qte_reserve/qte_dispo))*100;
END