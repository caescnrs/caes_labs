CREATE DEFINER=`sa`@`` FUNCTION `wp_get_resa_func`(
    pid_catalogue int(11),
    pid_type_destination int(11),
    pid_destination int(11),
    pid_hebergement int(11),
    pdate date
) RETURNS int(11)
BEGIN
    declare pqte int(11) default 0;
    
select t.db_date, r.id_catalogue, r.id_type_destination, r.id_destination, c.id_periode, c.id_hebergement,
`get_qte_dispo_func2`(r.id_catalogue, r.id_type_destination, r.id_destination, c.id_periode, c.id_hebergement, t.db_date) as qte_org,
sum(c.qte) as qte_resa
from time_dimension t
inner join (vac_resa2 r inner join vac_resa_conf2 c on c.stece = r.stece and c.dossier = r.dossier) 
on t.db_date between r.date_deb_resa and r.date_fin_resa
where t.db_date between '2018-07-22' and '2018-07-29' and r.id_destination = 657 
group by t.db_date, r.id_catalogue, r.id_type_destination, r.id_destination, c.id_periode, c.id_hebergement
order by t.db_date, c.id_periode, c.id_hebergement;

    if isnull(pqte) then
        set pqte= 0;
    end if;
RETURN pqte;
END