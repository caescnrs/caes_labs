CREATE FUNCTION `wp_get_tarif_enfant_func`(
    p_id_association int(11),
    p_type varchar(30),
    p_date_naissance date
) RETURNS int(11)
BEGIN
    declare tx_reduction int(11);
    
    SELECT te.tx_reduction into tx_reduction
        FROM vac_association_tenf2 te
        INNER JOIN vac_tarifs_enfants2 e ON e.id=te.id_tarif_enfant and e.exterieur = (IF (p_type='EXTERIEUR', 1, 0)) 
        and IF(e.unite='ANNEE', TIMESTAMPDIFF(YEAR, p_date_naissance, NOW())> e.tranche_inferieure  and TIMESTAMPDIFF(YEAR,p_date_naissance, NOW())< e.tranche_superieure,
        TIMESTAMPDIFF(MONTH, p_date_naissance, NOW())> e.tranche_inferieure  and TIMESTAMPDIFF(MONTH,p_date_naissance, NOW())< e.tranche_superieure)
        WHERE te.id_association=p_id_association;
    
    if isnull(tx_reduction) then
        set tx_reduction = 0;
    end if;
RETURN tx_reduction;
END