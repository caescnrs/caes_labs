CREATE DEFINER=`sa`@`` PROCEDURE `sp_wp_get_tarifs_enfants_partenaires2`(
p_asso INT(11)
)
BEGIN
    SELECT CONCAT('de ', te.tranche_inferieure, ' à moins de ', te.tranche_superieure, ' ans') as libelle,
        IF(e.tx_reduction>0, ROUND(ta.tarif_ref_location*(100- e.tx_reduction)/100,2), e.tarif_location) as tarif_a_payer,
        IF(e.tx_reduction>0, ROUND(ta.tarif_ref_dp*(100- e.tx_reduction)/100,2), e.tarif_dp) as px_demi_pension,
        IF(e.tx_reduction>0, ROUND(ta.tarif_ref_pc*(100- e.tx_reduction)/100,2), e.tarif_pc) as px_pension_cplete
    FROM vac_association_tenf2 e
    INNER JOIN vac_association_cata2 c on c.id=e.id_association and c.id=209
    INNER JOIN vac_tarifs_enfants2 te on te.id=e.id_tarif_enfant and te.exterieur=1
    INNER JOIN vac_tarifs_adultes2 ta on ta.id=c.id_tarif_adulte;
END