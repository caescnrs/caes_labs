/*
 *
 * DÉFINITION DES VARIABLES
 *
 *
var type;
var reponse;
var question;

/*
 *
 * FONCTION QUI DEFINIT LE STYLE DU CAPTCHA
 *
 *
function definirStyleCaptcha(){
	// On définit un chiffre entre 0 et 10
	return parseInt(parseInt(Math.random()*100)/10);
}

/*
 *
 * FONCTION QUI DEFINIT LA REPONSE
 *
 *
function definirReponse(){
	var image = document.getElementById("captcha");
	var enonce = document.getElementById("enonceCaptcha");
	var mot;
	
	var nb1 = parseInt(Math.random()*100)+1;
	var nb2 = parseInt(Math.random()*100)+1;
	var nb3 = parseInt(Math.random()*100)+1;
	
	switch(type){
		case 0:// Addition
			question = nb1 + " + " + nb2 + " = ?";
			enonce.innerHTML = question;
			return nb1+nb2;
		
		case 1:// Multiplication
			question = nb1 + " * " + nb2 + " = ?";
			enonce.innerHTML = question;
			return nb1*nb2;
		
		case 2:// Soustraction
			question = nb1 + " - " + nb2 + " = ?";
			enonce.innerHTML = question;
			return nb1-nb2;
		
		case 3:// Division
			question = nb1 + " / " + nb2 + " = ?<br/>(valeur entière, par exemple si vous trouvez 12.74 il faudra entrer 12 en réponse)";
			enonce.innerHTML = question;
			return parseInt(nb1/nb2);
		
		case 4:// Recopiage de mot existant
			mot = definirMotExistant(); 
			question = "Recopiez le mot : " + mot;
			enonce.innerHTML = question;
			return mot;
		
		case 5:// Recopiage de mot généré au pif
			mot = genererMot(); 
			question = "Recopiez le mot : " + mot;
			enonce.innerHTML = question;
			return mot;
		
		case 6:// Image
			question = "a";
			question = "Que voyez-vous?";
			enonce.innerHTML = question;
			return genererImage();
		
		case 7:// Question logique
			return genererQuestion();
		
		case 8:// Addition + Soustraction
			question = nb1 + " + " + nb2 + " - " + nb3 + " = ?";
			enonce.innerHTML = question;
			return nb1+nb2-nb3;
		
		case 9:// Multiplication + Addition
			question = nb1 + " * " + nb2 + " + " + nb3 + " = ?";
			enonce.innerHTML = question;
			return nb1*nb2+nb3;
		
		default:// Addition + Multiplication
			question = nb1 + " + " + nb2 + " * " + nb3 + " = ?";
			enonce.innerHTML = question;
			return nb1+nb2*nb3;
	}
}

/*
 *
 * FONCTION QUI DONNE UN MOT EXISTANT
 *
 *
function definirMotExistant(){
	var dictionnaire = [
		//A
		"Abeille",
		"Asticot",
		"Anticonstitution",
		//B
		"Babouin",
		"Blaireau",
		"Betterave",
		"Boisson",
		"Biologique",
		"Bière",
		//C
		"Calcul",
		"Calculatrice",
		"Calculer",
		"Calle-porte",
		"Claquage",
		"Clocher",
		"Consitution",
		"Corruption",
		"Courage",
		"Crevaison",
		//D
		"Douche",
		"Douillet",
		"Dortoir",
		"Dromadaire",
		"Dactylographie",
		"Disséminer",
		"Disculpter",
		"Discuter",
		"Devoir",
		"Douleur",
		"Dyslexie",
		"Direction",
		"Dragon",
		"Dramaturge",
		//E
		"Enfant",
		"Espérance",
		"Etiquette",
		"Enfance",
		"Elephant",
		"Enivrance",
		"Entourage",
		//F
		"Fosse",
		"Festival",
		"File",
		"Fille",
		"Fil",
		"Fils",
		"Femme",
		"Floraison",
		"Fleur",
		"Farine",
		"Frigide",
		"Fromage",
		"Fort",
		"Fortification",
		//G
		"Grille",
		"Gazon",
		"Gaz",
		"Gallerie",
		"Grue",
		"Groin",
		"Garage",
		"Gorge",
		"Gros",
		//H
		"Hamster",
		"Herbe",
		"Herboriste",
		"Haricot",
		"Hermine",
		"Hachoir",
		"Hache",
		"Histoire",
		"Hommage",
		"Homme",
		"Hominidé",
		"Humanité",
		"Hétérogène",
		"Homogène",
		"Homo-Sapiens-Sapiens",
		//I
		"Irrégulier",
		"Inquisition",
		//J
		"Joie",
		"Joule",
		"Justice",
		"Jeu",
		"Joli",
		//K
		"Kaki",
		"Kiwi",
		//L
		"Laitue",
		"Loup",
		"Lapin",
		"Lit",
		"Lac",
		//M
		"Moine",
		"Message",
		"Mort",
		"Macaque",
		"Massue",
		"Matériel",
		"Marqueur",
		"Métaphore",
		"Méticuleux",
		"Martial",
		"Marital",
		"Marteau",
		"Mine",
		"Mineur",
		"Moisson",
		"Musique",
		"Mystique",
		//N
		"Ninja",
		"Noyade",
		"Nez",
		"Notaire",
		"Nourrice",
		"Nourriture",
		"Nature",
		"Naturalisation",
		"Nutrition",
		"Naturellement",
		//O
		"Oiseau",
		"Ordinateur",
		"Ordination",
		"Opérateur",
		"Os",
		"Oméga",
		"Olive",
		//P
		"Pastèque",
		"Pistolet",
		"Pomme de terre",
		"Prune",
		"Parapluie",
		"Pack",
		"Parasol",
		"Parre-soleil",
		"parallélépipède",
		"Professionnel",
		"Poisson",
		//Q
		"Quotient",
		"Quand",
		"Quiche",
		//R
		"Roue",
		"Roux",
		"Rature",
		"Rossignol",
		"Raisin",
		"Rhum",
		//S
		"Sachet de patates",
		"Sucre",
		"Sucette",
		"Salopette",
		"Salive",
		"Salve",
		"Source",
		"Salami",
		"Soupe",
		"Soirée",
		"Système",
		//T
		"Terroir",
		"Terre",
		"Trou",
		"Terraformation",
		"Torréfaction",
		"Tristesse",
		"Tuile",
		"Tempête",
		"Tropical",
		"Tropiques",
		"Tardivement",
		"Touche",
		"Tirer",
		//U
		"Unité",
		"Ukulele",
		"Unicellulaire",
		"Uniforme",
		//V
		"Voilier",
		"Voleur",
		"Vitre",
		"Vendre",
		"Visite",
		"Vestibule",
		"Vénérable",
		"Ville",
		"Vitrine",
		"Vache",
		//W
		"Wagon",
		"Weekend",
		//X
		"Xylophage",
		"Xylophone",
		//Y
		"Yacht",
		"Yaourt",
		//Z
		"Zèbre",
	]
	return dictionnaire[parseInt(Math.random()*100) % dictionnaire.length];
}

/*
 *
 * FONCTION QUI GENERE UN MOT
 *
 *
function genererMot(){
	// 8 <= nbLettre <= 15
	var nbLettre = (parseInt(Math.random()*100) % 8) + 8;
	var mot = "";
	
	for(var i = 0; i<nbLettre; i++){
		// 0 <= typeLettre <= 2
		var typeLettre = parseInt(Math.random()*100) % 3;
		
		if(typeLettre == 0){//Majuscule (65 -> 90)
			mot += String.fromCharCode((parseInt(Math.random()*100) % 26) + 65);
		} else if(typeLettre == 1) {//Minuscule (97 -> 122)
			mot += String.fromCharCode((parseInt(Math.random()*100) % 26) + 97);
		} else {// Chiffre (48 -> 57)
			mot += String.fromCharCode((parseInt(Math.random()*100) % 10) + 48);
		}
	}
	
	return mot;
}

/*
 *
 * FONCTION QUI GENERE UNE IMAGE
 *
 *
function genererImage(){
	var images = [
		["chat.png","chat"],
		["chien.png","chien"],
		["panda.png","panda"],
		["bureau.png","bureau"],
		["table.png","table"],
		["chaise.png","chaise"],
		["carotte.png","carotte"],
		["souris.png","souris"],
		["telephone.png","telephone"],
		["cheval.png","cheval"]
	]
	
	var indice = parseInt(Math.random()*100) % images.length;
	
	//document.getElementById("captcha").innerHTML = images[indice][0];
	document.getElementById("captcha").innerHTML = "<img src='/static/img/captcha/"+images[indice][0]+"' alt='captcha' style='width:100px;'></img>";
	return images[indice][1];
}

/*
 *
 * FONCTION QUI GENERE UNE QUESTION ET RETOURNE UNE REPONSE
 *
 *
function genererQuestion(){
	var questionReponse = [
		["De quelle couleur est la montre rouge?", "rouge"],
		["De quelle couleur est le cheval blanc?", "blanc"],
		["De quelle couleur est la pomme rouge?", "rouge"],
		["De quelle couleur est la pomme verte?", "verte"],
		["Combien de faces a un cube? (en toutes lettres)", "six"],
		["Combien de mois y a-t'il dans une année? (en toutes lettres)","douze"],
		["Combien y a-t'il de 's' dans la phrase 'Saute sur ses sabliers, ton temps est compté.' ? (en toutes lettres)","sept"],
		["Quelle heure sera-t'il demain à 12h51?","12h51"],
		["Écrivez 3 en toutes lettres", "trois"]
		["Corrigez l'orthographe de ce mot : nuaje", "nuage"],
		["De quelle couleur est le chocolat blanc?", "blanc"],
	]
	var indice = parseInt(Math.random()*100) % questionReponse.length;
	
	question = questionReponse[indice][0];
	document.getElementById("enonceCaptcha").innerHTML = question;
	
	return questionReponse[indice][1];;
}

/*
 *
 * FONCTION APPELÉE PAR LE BOUTON SUBMIT
 *
 *
function controleCaptcha(){
	var longueur = document.getElementById("question").value.length;
	var enonce = document.getElementById("enonceCaptcha");
	var reponseUtilisateur = document.getElementById("reponseCaptcha");

	var questionOK = false;
	var reponseOK = false;
	
	// On remet l'énoncé à sa valeur d'origine
	enonce.innerHTML = question;
	
	// Contrôle de la longueur de la question
	if(longueur > 0){
		questionOK = true;
	}
	else {
		enonce.innerHTML += "<br/>Veuillez saisir une question.";
	}
	
	// Contrôle de la réponse
	if(isNaN(reponse)) {// Lettre
		if(type==4 || type==5) {// Avec respect de la casse (pour recopiage)
			if(reponseUtilisateur.value == reponse){
				reponseOK = true;
			}
			else {
				enonce.innerHTML += "<br/>Réponse fausse.";
			}
		} else {// Sans respect de la casse (pour autre)
			if(reponseUtilisateur.value.toLowerCase().indexOf(reponse.toLowerCase()) != -1){
				reponseOK = true;
			}
			else {
				enonce.innerHTML += "<br/>Réponse fausse.";
			}
		}
	} else if(!isNaN(reponse)) {// Chiffre
		if(parseInt(reponseUtilisateur.value) == reponse){
			reponseOK = true;
		}
		else {
			enonce.innerHTML += "<br/>Réponse fausse.";
		}
	} else {
		enonce.innerHTML += "<br/>Réponse fausse.";
	}
	
	return questionOK && reponseOK;
}

/*
 *
 * AFFECTATION DES VARIABLES
 *
 *
type = definirStyleCaptcha();
reponse = definirReponse();*/

/*
 *
 * GOOGLE API RECAPTCHA
 *
 */
 
var questionOK = false;
var captchaOK = false;

function controleCaptcha(){
	// On regrade si l'utilisateur a fait le captcha
	var a = document.getElementById("");
	var enonce = document.getElementById("enonceCaptcha");
	
	// On remet l'énoncé à zéro
	enonce.innerHTML = "";
	
	// On contrôle si quelque chose a été entré dans la réponse
	if( document.getElementById("question").value.length > 0 ){
		// On contrôle si un point d'interrogation a été mis à la fin
		if( document.getElementById("question").value.indexOf("?") == document.getElementById("question").value.length - 1 ){
			questionOK = true;
		} else {
			enonce.innerHTML += "Veuillez mettre un point d'interrogation à la fin de votre question<br/>";
			questionOK = false;
		}
	} else {
		enonce.innerHTML += "Veuillez saisir une question<br/>";
		questionOK = false;
	}
	
	// On contrôle si le captcha est bon
	if( grecaptcha.getResponse(a).length > 0 ){
		captchaOK = true;
	} else {
		enonce.innerHTML += "Veuillez remplir le captcha<br/>";
	}
	
	// On envoie le formulaire si tout est OK
	if(captchaOK && questionOK){
		var btn = document.getElementById("form.asked");
		var evt = document.createEvent("MouseEvents");
		
		// On clique sur le submit
		evt.initMouseEvent("click", true, true, window,0, 0, 0, 0, 0, false, false, false, false, 0, null);
		btn.dispatchEvent(evt);
	}
	
	return false;
}

// Fonction pour inhiber l'action de la touche entrée
function toucheEntree(event){
	if(!event && window.event){
		event = window.event;
	}
	
	if(event.keycode == 13){
		event.returnValue = false;
		event.cancelBubble = true;
		controleCaptcha();
	}
	
	if(event.which == 13){
    event.preventDefault();
    event.stopPropagation();
    controleCaptcha();
	}
}
