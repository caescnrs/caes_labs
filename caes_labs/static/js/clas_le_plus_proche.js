/*
 *
 * Affectation des variables
 *
 */
// XMLHttpRequests
var req1 = new XMLHttpRequest();
var req2 = new XMLHttpRequest();

// Résultats
var results1;
var results2;

// Marqueurs
var m1, m2, m3;

// Points
var point1 = new google.maps.LatLng(0.0, 0.0);
var point2 = new google.maps.LatLng(0.0, 0.0);
var point3 = new google.maps.LatLng(0.0, 0.0);
var point4 = new google.maps.LatLng(0.0, 0.0);

// Zone des Marqueurs
var zoneMarqueurs;

// Adresses
var adresse1 = "";
var adresse3 = "";
var adresse4 = "";

// Listes
var clasListe = [];
var resListe = [];
var tab = [];

// Booléens
var INCREMENT = 0;
var fini = false;

var voiture = true;
var velo = false;
var marche = false;
var is_m1 = true;
var is_m2 = false;
var is_m3 = false;

// Les icones
var icone1, icone2, icone3;

/*
 *
 * Fonctions utiles à la page
 *
 */
// Fonction qui va retourner le tableau des Clas généré via le select listeClas
function get_liste(){
	res = [];
	liste = document.getElementById("listeClas");
	
	for(var i = 0; i<liste.length; i++){
		liste.selectedIndex = i;// On va à l'élément suivant du select
		value = liste.value.split("_/_");
		
		dictionnaire = {};// On stocke dans un dictionnaire
		dictionnaire['no_clas']=value[0];
		dictionnaire['no_region']=value[1];
		dictionnaire['libelle']=value[2];
		dictionnaire['adres1']=value[3];
		dictionnaire['adres2']=value[4];
		dictionnaire['code_postal']=value[5];
		dictionnaire['ville']=value[6];
		dictionnaire['code_pays']=value[7];
		dictionnaire['no_tel']=value[8];
		dictionnaire['mail']=value[9];
		dictionnaire['longitude']=parseFloat(value[10]);
		dictionnaire['latitude']=parseFloat(value[11]);
		
		res.push(dictionnaire);
	}
	
	return res;
}

/*
 *
 * Fonction qui appelle la page distante des APIs
 *
 */
function chercher(){
	// On met l'image de chargement au début
	var img = document.getElementById("image").innerHTML;
	document.getElementById("res").innerHTML = "<img src='" + img + "' style='width:100px'></img>";
	
	// On cache la map et les moyens de locomotion
	document.getElementById("moyenLocomotion").style.display = "none";
	document.getElementById("map").style.display = "none";

	// On affecte les variables utiles	
	var adresse = document.getElementById("adresse").value;
	var departement = document.getElementById("departement").value;
	var ville = document.getElementById("ville").value;

	resListe = [];
	clasListe = get_liste();
	INCREMENT = 0;
	fini = false;
	
	icone1 = {
		url: document.getElementById("icone1").innerHTML,
		size: new google.maps.Size(32,45),
		anchor: new google.maps.Point(17,45)
	};

	icone2 = {
		url: document.getElementById("icone2").innerHTML,
		size: new google.maps.Size(32,45),
		anchor: new google.maps.Point(17,45)
	};

	icone3 = {
		url: document.getElementById("icone3").innerHTML,
		size: new google.maps.Size(32,45),
		anchor: new google.maps.Point(17,45)
	};
	
	adresse1 = adresse + " " + departement + " " + ville;
	
	// On lance la première recherche
	recherche(adresse1);
}

// Fonction qui lance la recherche via l'API de adresse.data.gouv.fr
function recherche(adresse1){
	req1.open("GET", "http://api-adresse.data.gouv.fr/search/?q="+adresse1,true);
	req1.onreadystatechange = calcul;
	req1.send(null);
}

/*
 *
 * Fonction CALLBACK
 *
 */
function calcul(){
	if(req1.readyState == 4){
		// Première requête
		if(req1.responseText.length == 123+adresse1.length){// On contrôle si la requête ADRESSE UTILISATEUR ne renvoie rien
			document.getElementById("res").innerHTML = "<b style='color:red'>L'adresse que vous avez indiqué est incorrecte.<b>";
			document.getElementById("traitement").innerHTML = "";
			document.getElementById("clas").innerHTML = "";
		} else {
			results1 = eval('(' + req1.responseText + ')');
			point1 = new google.maps.LatLng(results1.features[0].geometry.coordinates[1], results1.features[0].geometry.coordinates[0]);
		
			var clas;
			
			for(INCREMENT; INCREMENT<clasListe.length; INCREMENT++){
				clas = clasListe[INCREMENT];
			
				// On affiche le nombre de clas traités
				var gFin = 200;
				var b = Math.floor(gFin - (resListe.length*gFin/clasListe.length)) % gFin;
				var g = Math.floor(resListe.length*gFin/clasListe.length) % gFin;
				document.getElementsByClassName("row")[1].backgroundColor="red";
				document.getElementById("traitement").innerHTML = "Distances calculées : <b style='color:rgb(0,"+g+","+b+")'>" + INCREMENT + "</b>/<b style='color:rgb(0,"+gFin+",0)'>" + clasListe.length+"</b>";
				
				// On affiche le nom du clas traité
				document.getElementById("clas").innerHTML = clas.adres1;
				point2 = new google.maps.LatLng(clas.longitude,clas.latitude);
		
				// Calcul de la distance
				var distance = google.maps.geometry.spherical.computeDistanceBetween(point1, point2);
				distance = distance/1000;// On prend le nombre de km
				distance = Math.floor(distance*100)/100;//On réduit à deux chiffres après la virgule
		
				var tableau = [];
				tableau.push(INCREMENT);
				tableau.push(distance);
		
				resListe.push(tableau);
			}
			//INCREMENT++;
		
			aff();
		}
	}
}

/*
 *
 * Fonction qui affiche le tableau des trois meilleurs
 *
 */
function aff(){
	if(resListe.length == clasListe.length){
		fini = true;
		// On efface le nombre de clas traités
		document.getElementById("traitement").innerHTML = "";
		document.getElementById("clas").innerHTML = "";
		// On trie le tableau resListe (il se retrouve en un grand tableau avec toute les valeurs en vrac
		resListe.sort(function(a,b){
			if(a[1] > b[1]){
				return 1;
			}
			if(a[1] < b[1]){
				return -1;
			}
			return 0;
		});
		// On prend les 3 premiers éléments du tableau
		tab = [];
		for(var i = 0; i < 3; i++){
			tab.push(resListe[i]);
		}
		resListe = tab;
		
		// On va chercher ces trois CLAS dans le tableau clasListe
		tab = [];
		for(var i = 0; i < resListe.length; i++){// On va prendre le ième élément de clasListe
			tab.push(clasListe[resListe[i][0]]);
		}
		
		// On affiche
		var res = document.getElementById("res");
		res.innerHTML = "<br/><br/>";
		for(var i = 0; i < tab.length; i++){
			var a = i+1;
			var titre = "<b>" + a + " - " +tab[i].libelle+"</b>";
			res.innerHTML += titre;
			res.innerHTML += "<br/>";

			if(tab[i].adres1.length > 3){
				res.innerHTML += tab[i].adres1;
				res.innerHTML += "<br/>";
			}
			
			if(tab[i].adres2.length > 3){
				res.innerHTML += tab[i].adres2;
				res.innerHTML += "<br/>";
			}
			
			res.innerHTML += tab[i].code_postal;
			res.innerHTML += " ";
			res.innerHTML += tab[i].ville;
			res.innerHTML += "<br/>";
			
			if(tab[i].no_tel.length > 3){
				res.innerHTML += "Téléphone : ";
				res.innerHTML += tab[i].no_tel;
				res.innerHTML += "<br/>";
			}
			
			if(tab[i].mail.length > 3){
				res.innerHTML += tab[i].mail;
			res.innerHTML += "<br/>";
			}
			
			res.innerHTML += "Distance : ";
			res.innerHTML += resListe[i][1];
			res.innerHTML += " km";
			
			res.innerHTML += "<br/><br/><br/>";
		}
		
		// On affiche la map et les moyens de locomotion
		document.getElementById("moyenLocomotion").style.display = "block";
		document.getElementById("map").style.display = "block";
		map = new google.maps.Map(document.getElementById("map"), {
			zoom: 12,
			center: point1,
			mapTypeId: google.maps.MapTypeId.ROADMAP
		});
		document.getElementById("map").style.width = '100%';
		document.getElementById("map").style.height = '750px';
		
		// On définit ce qui va afficher l'itinéraire
		direction = new google.maps.DirectionsRenderer({
			map: map,
			suppressMarkers: true
		});
		
		// On met les points sur la carte
			// 0 - La position du bonhomme
			pos = new google.maps.Marker({
				position: point1,
				title: adresse1,
				map: map
			});
			
			// On lance le placement des points
			adresse2 = tab[0].adres1 + " " + tab[0].code_postal + " " + tab[0].ville;
			adresse3 = tab[1].adres1 + " " + tab[1].code_postal + " " + tab[1].ville;
			adresse4 = tab[2].adres1 + " " + tab[2].code_postal + " " + tab[2].ville;
			
			point2 = new google.maps.LatLng(tab[0].longitude,tab[0].latitude);
			point3 = new google.maps.LatLng(tab[1].longitude,tab[1].latitude);
			point4 = new google.maps.LatLng(tab[2].longitude,tab[2].latitude);
			
			// On attend la fin des deux étapes précédentes pour afficher les deux derniers points
			// 1 - La position du premier point
			m1 = new google.maps.Marker({
				position: point2,
				title: adresse2,
				icon: icone1,
				map: map
			});
			
			// 2 - La position du deuxième point
			m2 = new google.maps.Marker({
				position: point3,
				title: adresse3,
				icon: icone2,
				map: map
			});
	
			// 3 - La position du troisième point
			m3 = new google.maps.Marker({
				position: point4,
				title: adresse4,
				icon: icone3,
				map: map
			});
			
			// On réduit assez pour afficher les 3 boutons
			zoneMarqueurs = new google.maps.LatLngBounds();
			zoneMarqueurs.extend(pos.getPosition());
			zoneMarqueurs.extend(m1.getPosition());
			zoneMarqueurs.extend(m2.getPosition());
			zoneMarqueurs.extend(m3.getPosition());
			
			map.fitBounds(zoneMarqueurs);
			
			// Définition des listeners
				// Click gauche
				google.maps.event.addListener(m1, 'click', function(){
					itineraire(point1,point2);
				});
				google.maps.event.addListener(m2, 'click', function(){
					itineraire(point1,point3);
				});
				google.maps.event.addListener(m3, 'click', function(){
					itineraire(point1,point4);
				});
				// Click droit
				google.maps.event.addListener(map, 'rightclick', function(){
					map.fitBounds(zoneMarqueurs);
				});
				// MouseOver
				var info1 = new google.maps.InfoWindow({
					content: "<div id='content'>Distance : "+resListe[0][1]+" km</div>"
				});
				var info2 = new google.maps.InfoWindow({
					content: "<div id='content'>Distance : "+resListe[1][1]+" km</div>"
				});
				var info3 = new google.maps.InfoWindow({
					content: "<div id='content'>Distance : "+resListe[2][1]+" km</div>"
				});
				google.maps.event.addListener(m1, 'mouseover', function(){
					info1.open(map, m1);
					//m1.setAnimation(google.maps.Animation.BOUNCE);
				});
				google.maps.event.addListener(m2, 'mouseover', function(){
					info2.open(map, m2);
					//m2.setAnimation(google.maps.Animation.BOUNCE);
				});
				google.maps.event.addListener(m3, 'mouseover', function(){
					info3.open(map, m3);
					//m3.setAnimation(google.maps.Animation.BOUNCE);
				});
				// MouseOut
				google.maps.event.addListener(m1, 'mouseout', function(){
					info1.close(map, m1);
					//m1.setAnimation(null);
				});
				google.maps.event.addListener(m2, 'mouseout', function(){
					info2.close(map, m2);
					//m2.setAnimation(null);
				});
				google.maps.event.addListener(m3, 'mouseout', function(){
					info3.close(map, m3);
					//m3.setAnimation(null);
				});
	} else {
	}
}

// Fonction de calcul de l'initénaire
function itineraire(origin, destination){
	// On contrôle le radiobutton qui est coché (au cas où l'utilisateur a fait F5 et que le bouton VOITURE n'est pas coché
	var btn = document.getElementsByName("moyenLocomotion");
	if(btn[1].checked){// Vélo coché
		voiture = false;
		velo = true;
		marche = false;
	} else if(btn[2].checked){// Marche cochée
		voiture = false;
		velo = false;
		marche = true;
	} else {// Voiture cochée (par défaut)
		voiture = true;
		velo = false;
		marche = false;
	}
	// On re-contrôle le radiobutton qui est coché
	var mode;
	if(voiture == true){
		mode = "DRIVING";
	} else if(velo == true){
		mode = "BICYCLING";
	} else if(marche == true){
		mode = "WALKING";
	}
	
	// On change la valeur de is_m[n°]
	var TEMPS = 7000;
	if(destination == m1.position){
		is_m1 = true;
		is_m2 = false;
		is_m3 = false;
		if(m1.animation == null){
			m1.setAnimation(google.maps.Animation.BOUNCE);
			m2.setAnimation(null);
			m3.setAnimation(null);
			setTimeout(function(){m1.setAnimation(null)}, TEMPS);// Timer de TEMPS secondes pour l'animation
		}
	} else if(destination == m2.position){
		is_m1 = false;
		is_m2 = true;
		is_m3 = false;
		if(m2.animation == null){
			m1.setAnimation(null);
			m2.setAnimation(google.maps.Animation.BOUNCE);
			m3.setAnimation(null);
			setTimeout(function(){m2.setAnimation(null);}, TEMPS);
		}
	} else if(destination == m3.position){
		is_m1 = false;
		is_m2 = false;
		is_m3 = true;
		if(m3.animation == null){
			m1.setAnimation(null);
			m2.setAnimation(null);
			m3.setAnimation(google.maps.Animation.BOUNCE);
			setTimeout(function(){m3.setAnimation(null);}, TEMPS);
		}
	}
	
	var request = {
		origin: origin,
		destination: destination,
		travelMode: google.maps.TravelMode[mode]
	}
	var directionsService = new google.maps.DirectionsService();
	directionsService.route(request, function(response, status){
		if(status == google.maps.DirectionsStatus.OK){
			direction.setDirections(response);
		}
	});
}

// Fonction de changement de mode de locomotion
function locomotion(mode){
	// On contrôle le mode
	if(mode == "D"){
		voiture = true;
		velo = false;
		marche = false;
	} else if(mode == "B"){
		voiture = false;
		velo = true;
		marche = false;
	} else if(mode == "W"){
		voiture = false;
		velo = false;
		marche = true;
	}
	
	// On contrôle la destination
	if(is_m1){
		itineraire(point1, point2);
	} else if(is_m2){
		itineraire(point1, point3);
	} else if(is_m3){
		itineraire(point1, point4);
	}
}

function controleDebut(){
	var a = document.getElementById("adresse").value.length;
	var c = document.getElementById("departement").value.length;
	var v = document.getElementById("ville").value.length;
	if(a>0 || c>0 || v>0){
		chercher();
	}
}
