/*
 *
 * INITIALISATION DES VARIABLES
 *
 */
var mt_commande = 0.00;
var mt_subv = 0.00;
var X = 500;// X temps => 1000 ms = 1 secondes
var xhr = new XMLHttpRequest();
var stockInsuffisant_affiche = false;// Permet de savoir si on a déjà affiché le message "Stock insuffisant"

/*
 *
 * FONCTIONS APPELEES PAR LES BOUTONS
 *
 */
function calculer(){
	$("#cadre_commande").hide(X);
	
	setTimeout( function(){
		// On remet à zéro les deux variables globales
		mt_commande = 0.00;
		mt_subv = 0.00;
	
		// On prend toutes les quantités commandées
		select = document.getElementsByName("qte");
	
		for(var i = 0; i < select.length; i++){
			// On prend la quantité commandée
			var qte_commande = parseFloat(select[i][select[i].selectedIndex].value);
			var prix = parseFloat(select[i].dataset.prix);
			var subv = parseFloat(select[i].dataset.mtce);
			var tdfam = parseFloat(document.getElementById("td_fam").innerHTML);
		
			// On incrémente les deux montants globaux
			mt_commande += qte_commande * prix;
			
			if(select[i].dataset.tdfam == '0'){
				mt_subv += qte_commande * subv;
			} else {
				mt_subv += (qte_commande * subv) + (tdfam * (qte_commande * prix))/100;
			}
		}
		
		// On passe les montants des commandes et les subventions à 2 chiffres et à round
		mt_subv = Math.round(mt_subv*100)/100;
		mt_commande = Math.round(mt_commande*100)/100;
		var tot = parseInt((mt_commande - mt_subv)*100)/100;
	
		document.getElementById("mt_commande").innerHTML = mt_commande + " €";
		document.getElementById("mt_subv").innerHTML = mt_subv + " €";
		document.getElementById("tot_commande").innerHTML = tot + " €";
		
		if(mt_commande - mt_subv > 0){
			$("#regler_bouton").removeProp('disabled');
		} else {
			$("#regler_bouton").prop('disabled','true');
		}
		
		controleDisponibilite();
		
		$("#cadre_commande").show(X);
	}, X);
}

function regler(){
	controleDisponibilite();
	alert("== Pas encore implémenté ==\n\
	À faire :\n\
	1) Création de la facture:\n\
	\t- mvt_billets\n\
	\t- lig_billets\n\
	2) Mettre code en facture\n\
	3) Envoyer sur PAYBOX\
	");
}

/*
 *
 * XMLHttpRequest pour la disponibilité
 *
 */
setInterval("appelXHR()", 10000);

// Appel du xhr.pt
function appelXHR(){
	xhr.open("GET", "/billetterie_xhr",true);
	xhr.onreadystatechange = actualiserDisponibilite;
	xhr.send(null);
}

/*
 *
 * FONCTION CALLBACK
 *
 */
// Actualisation de la disponibilité
function actualiserDisponibilite(){
	if(xhr.readyState == 4 && xhr.status==200){
		var res = eval('(' + xhr.responseText + ')');
		
		// On parcourt la liste de billets
		for(var i=0; i<res.billets.length; i++){
			// On change la valeur de la disponibilité si l'ID existe
			if(document.getElementById(res.billets[i].libelle)){
				document.getElementById(res.billets[i].libelle).innerHTML = res.billets[i].qte_dispo;
			}
		}
		
		controleDisponibilite();
	}
}

/*
 *
 * Fonction contrôlant la disponibilité
 *
 */
function controleDisponibilite(){
	// Si le bonhomme a demandé au moins un article et qu'il attend après avoir cliqué sur [Calculer]
	if(mt_commande > 0){
		var dispo = true;
		var commandes = document.getElementsByName("qte");
		
		for(var i=0; i<commandes.length; i++){
			// Si la qté commandée est supérieure à la quantité disponible
			if(parseInt(commandes[i][commandes[i].selectedIndex].value) > parseInt(document.getElementById(commandes[i].dataset.libelle).innerHTML)){
				dispo = false;
			}
		}
		
		// On grise ou pas
		if(dispo == true){
			$("#regler_bouton").removeProp('disabled');
			stockInsuffisant_affiche = false;
		} else {
			$("#regler_bouton").prop('disabled','true');
			if(stockInsuffisant_affiche == false){
				alert("Stock insuffisant");
				stockInsuffisant_affiche = true;
			}
		}
	}
}
