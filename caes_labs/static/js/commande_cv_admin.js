X = 500;

function charger(chemin){
	if(chemin!="NON"){
			$("#docScan").fadeOut(X);
			chemin = document.getElementById("chemin").innerHTML + chemin;
	
			// On charge le fichier et on l'affiche après X secondes
			setTimeout(function(){
				document.getElementById("docScan").src = chemin;
				$("#docScan").fadeIn(X);
			},X);
	} else {
			alert("L'agent n'a pas uploadé ce document.");
	}
}
