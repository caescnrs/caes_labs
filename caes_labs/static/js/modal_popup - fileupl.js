;

$(document).ready(function () {
	$('#afficheJustifModal').on('show.bs.modal', function (event) { // id of the modal with event
		var button = $(event.relatedTarget); // Button that triggered the modal
		var justif_id = button.data('justifid'); // Extract info from data-* attributes
		var justif_titre = button.data('justifname');

		// Update the modal's content.
		var modal = $(this);
		modal.find('.modal-title').text(justif_titre);
		modal.find('#imagesrc').attr('src',justif_id);	  

		});
	$('#uploadJustifModal').on('show.bs.modal', function (event) { // id of the modal with event
		var button = $(event.relatedTarget); // Button that triggered the modal
		var justif_id = button.data('justifid'); // Extract info from data-* attributes
		var justif_titre = button.data('justifname');
		var justif_lib = button.data('justiflib');
		var justif_matricule = button.data('justifmatricule');

		// Update the modal's content.
		var modal = $(this);
		modal.find('.modal-title').text('Envoi de ' + justif_titre);
		modal.find('.modal-lib').text(justif_lib);
		// modal.find('#iframesrc').attr('src',justif_id);	  

		});
});

