$(document).ready(function () {

    $(".select_bill").change(
        function () {
            if ($(".select_bill:checked").length) {
                $('button#calculate').removeProp('disabled');
            } else {
                $('button#calculate').prop('disabled', true);
            }
    });

});
