/*
 *
 * FONCTION QUI DEFINIT LE STYLE DU CAPTCHA
 *
 */
function definirStyleCaptcha(){
	// On définit un chiffre entre 0 et 10
	return parseInt(parseInt(Math.random()*100)/10);
}

/*
 *
 * FONCTION QUI DEFINIT LA REPONSE
 *
 */
function definirReponse(){
	var image = document.getElementById("captcha");
	var enonce = document.getElementById("enonceCaptcha");
	
	var nb1 = parseInt(Math.random()*100)+1;
	var nb2 = parseInt(Math.random()*100)+1;
	var nb3 = parseInt(Math.random()*100)+1;
	
	switch(type){
		case 0:// Addition
			enonce.innerHTML = nb1 + " + " nb2 + " = ?";
			return nb1+nb2;
		break;
		
		case 1:// Multiplication
			enonce.innerHTML = nb1 + " * " nb2 + " = ?";
			return nb1*nb2;
		break;
		
		case 2:// Soustraction
			enonce.innerHTML = nb1 + " - " nb2 + " = ?";
			return nb1-nb2;
		break;
		
		case 3:// Division
			enonce.innerHTML = nb1 + " / " nb2 + " = ?<br/>(valeur entière, par exemple si vous trouvez 12.74 il faudra entrer 12 en réponse)";
			return parseInt(nb1/nb2);
		break;
		
		case 4:// Recopiage de mot existant
			var mot = definirMotExistant(); 
			enonce.innerHTML = "Recopiez le mot : " + mot;
			return mot;
		break;
		
		case 5:// Recopiage de mot généré au pif
			var mot = genererMot(); 
			enonce.innerHTML = "Recopiez le mot : " + mot;
			return mot;
		break;
		
		case 6:// Image
		break;
		
		case 7:// Question logique
			return genererQuestion();
		break;
		
		case 8:// Addition + Soustraction
		break;
		
		case 9:// Multiplication + Addition
		break;
		
		case 10:// Addition + Multiplication
		break;
	}
}

/*
 *
 * FONCTION QUI DONNE UN MOT EXISTANT
 *
 */
function definirMotExistant(){
	return "Pastèque";
}

/*
 *
 * FONCTION QUI GENERE UN MOT
 *
 */
function genererMot(){
	return "SDfjremS48";
}

/*
 *
 * FONCTION QUI GENERE UNE QUESTION ET RETOURNE UNE REPONSE
 *
 */
function genererQuestion(){
	var enonce = document.getElementById("captcha");
	enonce.innerHTML = "Écris Oktamer";
	return "Oktamer";
}

var type = definirStyleCaptcha();
var reponse = definirReponse();
