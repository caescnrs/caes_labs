# -*- coding: utf8 -*-
from pyramid.response import Response
from pyramid.renderers import render, get_renderer
from pyramid.view import (
    view_config,
    view_defaults,
    forbidden_view_config,
)
from pyramid.security import (
    authenticated_userid,
    remember,
    forget,
)
from pyramid.httpexceptions import (
    HTTPFound,
    HTTPNotFound,
    HTTPForbidden,
)
from sqlalchemy.exc import DBAPIError
from .security import groupfinder
from pyramid_ldap3 import (
    get_ldap_connector,
)
from pyramid_mailer import get_mailer
from pyramid_mailer.message import Message, Attachment
from datetime import *
from dateutil.relativedelta import relativedelta

import transaction	
import collections
import json
from time import strftime

#Pour fileupload
import os
import shutil 

from datetime import *
from dateutil.relativedelta import relativedelta
from pyramid.security import (
    authenticated_userid,
)
from pyramid.view import (
    view_config,
    view_defaults,
    forbidden_view_config,
)
from .security import *
from .models.default import *


def isGestionnaire(request):
    logged_in = authenticated_userid(request)
    is_ok = False
    if logged_in is not None:
        groups = groupfinder(logged_in, request)
        if 'group:gestionnaires' in groups or 'group:sac' in groups or 'group:su' in groups or 'group:administrators' in groups:
            is_ok = True
    return is_ok