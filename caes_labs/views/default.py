# -*- coding: utf8 -*-
from pyramid.response import Response
from pyramid.renderers import render, get_renderer
from pyramid.view import (
    view_config,
    view_defaults,
    forbidden_view_config,
)
from pyramid.security import (
    authenticated_userid,
    remember,
    forget,
)
from pyramid.httpexceptions import (
    HTTPFound,
    HTTPNotFound,
    HTTPForbidden,
)
from sqlalchemy.exc import DBAPIError
from ..security import groupfinder
from pyramid_ldap3 import (
    get_ldap_connector,
)
from pyramid_mailer import get_mailer
from pyramid_mailer.message import Message, Attachment
from datetime import *
from dateutil.relativedelta import relativedelta

import transaction	
import collections
import json
import pypyodbc
from time import strftime

#Pour fileupload
import os
import shutil 
import pdfkit

from ..models.default import  *
from ..tools import *

#Author BBA : Page d'accueil
@view_config(route_name='home', renderer='../templates/default/home.pt', permission='view')
def home(request):
    logged_in = authenticated_userid(request)
    return {
        'page_title': u"Accueil",
    }

@view_config(route_name='boutons', renderer='/templates/default/boutons.pt', permission='view')
def boutons(request): 
    return {
        'page_title': u"Boutons",
    }


@view_config(route_name='csv', renderer='..//templates/default/csv.pt', permission='view')
def csv(request): 
    import csv
    filename = 'caes_labs/static/img/cesutest.csv'
    liste = []
    with open(filename) as csvfile:
        reader = csv.DictReader(csvfile)
        for row in reader:
            test = row[u'code']
    
    import pdb;pdb.set_trace()
    return {
        'page_title': u"Boutons",
        'test': test,
    }


#Author BBA : Page de login, obligatoire pour consulter toutes les autres pages
@view_config(route_name='login', renderer='../templates/default/login.pt', permission='view')
@view_config(route_name='login_as', renderer='../templates/default/login.pt', permission='view')
@forbidden_view_config(renderer='../templates/default/login.pt')
def login(request):
    current_route_path = request.current_route_path()
    if 'login_as' in current_route_path:
        login = request.matchdict['login']
        login_url = request.route_url('login_as', login=login)
    else:
        login = ''
        login_url = request.route_url('login')

    referrer = request.url

    if referrer == login_url:
        referrer = '/' # never use the login form itself as came_from

    came_from = request.params.get('came_from', referrer)

    password = ''
    if 'form.submitted' in request.params:
        login = request.params['login'].upper()
        password = request.params['password']
        connector = get_ldap_connector(request)
        data = connector.authenticate(login, password)
        if data is not None:				 
            groups = groupfinder(login, request)  
            if groups is not None:
                headers = remember(request, login)
                return HTTPFound(location=came_from, headers=headers)
        request.session.flash(u"L'identification a échoué, veuillez réessayer.")

    return {
        'page_title': u"Connexion",
        'url': login_url,
        'came_from': came_from,
        'login': login,
    }

#Author BBA : Pour se deconnecter
@view_config(route_name='logout')
def logout(request):
    request.session.invalidate()
    headers = forget(request)
    return HTTPFound(location=request.route_url('login', login=''),headers=headers)
