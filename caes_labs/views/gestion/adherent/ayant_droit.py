# -*- coding: utf8 -*-
from pyramid.response import Response
from pyramid.renderers import render, get_renderer
from pyramid.view import (
    view_config,
    view_defaults,
    forbidden_view_config,
)
from pyramid.security import (
    authenticated_userid,
    remember,
    forget,
)
from pyramid.httpexceptions import (
    HTTPFound,
    HTTPNotFound,
    HTTPForbidden,
)
from sqlalchemy.exc import DBAPIError
from pyramid_ldap3 import (
    get_ldap_connector,
)

import transaction	
import collections
import json
import pypyodbc
from time import strftime

#Pour fileupload
import os
import shutil 
import pdfkit


@view_config(route_name='test', renderer='../../../templates/default/test.pt', permission='view')
def test(request): 
    return {
        'page_title': u"Boutons",
    }

@view_config(route_name="ayant_droit_liste", renderer="../../../templates/gestion/adherent/ayant_droit/ayant_droit_liste.pt", permission="view")
def ayant_droit_liste(request):

    etat = 1	
    exercice = get_exercice()
    gestionnaire = "TOUT"	
    list_etat = get_list_etat()
    list_exercice = get_last_exercice()	
    list_gestionnaire = get_list_gestionnaire_search(request)		

    if 'etat' in request.params:
        etat = int(request.params["etat"])
    if 'exercice' in request.params:
        exercice = request.params["exercice"]
        exercice = int(request.params["exercice"])
    if 'gestionnaire' in request.params:
        gestionnaire = request.params["gestionnaire"]	
    ayant_droits = get_web_ayant_d_by_etat_exercice_gestionnaire(request,etat,exercice,gestionnaire)	
    list_ayant_droit=[]	

    for ayant_droit in ayant_droits:
        row = (ayant_droit.date_creat.strftime('%d/%m/%Y - %H:%M'), ayant_droit.date_modif.strftime('%d/%m/%Y - %H:%M'), ayant_droit.matricule, ayant_droit.nom, ayant_droit.prenom,ayant_droit.resp_maj, ayant_droit.id)
        list_ayant_droit.append(row)	

    return {
        'page_title': u"Gérer les ajouts d'ayant-droit",
        'etat' : etat,
        'exercice' : exercice,
        'gestionnaire': gestionnaire,
        'list_ayant_droit': json.dumps(list_ayant_droit),
        'list_etat': list_etat,
        'list_exercice': list_exercice,
        'list_gestionnaire': list_gestionnaire,
    }