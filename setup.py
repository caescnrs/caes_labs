import os

from setuptools import setup, find_packages

here = os.path.abspath(os.path.dirname(__file__))
with open(os.path.join(here, 'README.rst')) as f:
    README = f.read()
with open(os.path.join(here, 'CHANGES.txt')) as f:
    CHANGES = f.read()
    
requires = [
    'pyramid',
    'SQLAlchemy',
    'transaction',
    'pyramid_chameleon',
    'pyramid_debugtoolbar',
    'pyramid_layout',
    'pyramid_mailer',
    'pyramid_tm',
    'zope.sqlalchemy',
    'waitress',
    'mysqlclient == 1.3.12',
    'python-dateutil',
    'pdfkit',
    'pyexchange',
    'docutils',
    'pypyodbc',
    'ldap3',
    'pyramid_ldap3',
    'pymssql',
    ]

setup(name='caes_labs',
      version='0.1',
      description='caes_labs',
      long_description=README + '\n\n' + CHANGES,
      classifiers=[
        "Programming Language :: Python",
        "Framework :: Pyramid",
        "Topic :: Internet :: WWW/HTTP",
        "Topic :: Internet :: WWW/HTTP :: WSGI :: Application",
        ],
      author='',
      author_email='',
      url='',
      keywords='web wsgi bfg pylons pyramid',
      packages=find_packages(),
      include_package_data=True,
      zip_safe=False,
      test_suite='caes_labs',
      install_requires=requires,
      entry_points="""\
      [paste.app_factory]
      main = caes_labs:main
      [console_scripts]
      initialize_caes_labs_db = caes_labs.scripts.initializedb:main
      """,
      )
