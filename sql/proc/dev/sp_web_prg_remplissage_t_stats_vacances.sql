CREATE DEFINER=`sa`@`` PROCEDURE `sp_web_prg_remplissage_t_stats_vacances`()
BEGIN    
    DELETE FROM web_t_stats_vacances;
    CALL sp_web_remplissage_t_stats_vacances_by_code_activite('AUSSOIS');
    CALL sp_web_remplissage_t_stats_vacances_by_code_activite('FAMILLE');
    CALL sp_web_remplissage_t_stats_vacances_by_code_activite('FREJUS');
    CALL sp_web_remplissage_t_stats_vacances_by_code_activite('JEUNES');
    CALL sp_web_remplissage_t_stats_vacances_by_code_activite('OLERON');
    CALL sp_web_remplissage_t_stats_vacances_by_code_activite('PLANTIERS');
END