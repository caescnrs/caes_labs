CREATE DEFINER=`sa`@`localhost` PROCEDURE `sp_web_prg_remplissage_t_stats_nuitee`()
BEGIN    
    DELETE FROM web_t_stats_nuitee WHERE annee >= YEAR(DATE_ADD(current_date(), INTERVAL -2 YEAR));
    CALL `sp_prg_calcul_nuitees`('CNRS', 'CE', 'VACANCES', 'AUSSOIS', 
                                    CONCAT(YEAR(DATE_ADD(current_date(), INTERVAL -2 YEAR)), '-01-01'),
                                    CONCAT(YEAR(current_date()), '-12-31'), 'WEB');
    CALL sp_web_remplissage_t_stats_nuitee_by_code_activite('AUSSOIS');
    CALL `sp_prg_calcul_nuitees`('CNRS', 'CE', 'VACANCES', 'FAMILLE', 
                                    CONCAT(YEAR(DATE_ADD(current_date(), INTERVAL -2 YEAR)), '-01-01'),
                                    CONCAT(YEAR(current_date()), '-12-31'), 'WEB');
    CALL sp_web_remplissage_t_stats_nuitee_by_code_activite('FAMILLE');
    CALL `sp_prg_calcul_nuitees`('CNRS', 'CE', 'VACANCES', 'CLYTHIA', 
                                    CONCAT(YEAR(DATE_ADD(current_date(), INTERVAL -2 YEAR)), '-01-01'),
                                    CONCAT(YEAR(current_date()), '-12-31'), 'WEB');
    CALL sp_web_remplissage_t_stats_nuitee_by_code_activite('CLYTHIA');
    CALL `sp_prg_calcul_nuitees`('CNRS', 'CE', 'VACANCES', 'JEUNES', 
                                    CONCAT(YEAR(DATE_ADD(current_date(), INTERVAL -2 YEAR)), '-01-01'),
                                    CONCAT(YEAR(current_date()), '-12-31'), 'WEB');
    CALL sp_web_remplissage_t_stats_nuitee_by_code_activite('JEUNES');
    CALL `sp_prg_calcul_nuitees`('CNRS', 'CE', 'VACANCES', 'OLERON', 
                                    CONCAT(YEAR(DATE_ADD(current_date(), INTERVAL -2 YEAR)), '-01-01'),
                                    CONCAT(YEAR(current_date()), '-12-31'), 'WEB');
    CALL sp_web_remplissage_t_stats_nuitee_by_code_activite('OLERON');
    CALL `sp_prg_calcul_nuitees`('CNRS', 'CE', 'VACANCES', 'PLANTIERS', 
                                    CONCAT(YEAR(DATE_ADD(current_date(), INTERVAL -2 YEAR)), '-01-01'),
                                    CONCAT(YEAR(current_date()), '-12-31'), 'WEB');
    CALL sp_web_remplissage_t_stats_nuitee_by_code_activite('PLANTIERS');
END