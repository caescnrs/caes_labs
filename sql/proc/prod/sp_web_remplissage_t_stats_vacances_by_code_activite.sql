CREATE DEFINER=`sa`@`` PROCEDURE `sp_web_remplissage_t_stats_vacances_by_code_activite`(p_code_activite VARCHAR (20))
BEGIN    
    DECLARE c_year INT DEFAULT 0;    
    DECLARE v_year DOUBLE DEFAULT year(CURDATE());
    DECLARE v_mois INT DEFAULT 1;
    
    WHILE c_year < 3 DO
        SET v_mois = 1;
        WHILE v_mois <= 12 DO		
            INSERT INTO web_t_stats_vacances 
                (SELECT
                    v_year,
                    p_code_activite,
                    CASE v_mois
                        WHEN 1 THEN 'janvier'
                        WHEN 2 THEN 'fevrier'
                        WHEN 3 THEN 'mars'
                        WHEN 4 THEN 'avril'
                        WHEN 5 THEN 'mai'
                        WHEN 6 THEN 'juin'
                        WHEN 7 THEN 'juillet'
                        WHEN 8 THEN 'aout'
                        WHEN 9 THEN 'septembre'
                        WHEN 10 THEN 'octobre'
                        WHEN 11 THEN 'novembre'
                        ELSE 'decembre'  
                    END,
                    CONVERT(IF(f.date_facture, SUM(total_ht), 0), SIGNED INTEGER) ,
                    CONVERT(IF(f.date_facture, SUM(mt_particip_ce), 0) , SIGNED INTEGER) 
                FROM ganaelbd.vac_facture f
                where stece='CE' and code_activite=p_code_activite and YEAR(date_facture)=v_year and MONTH(date_facture)=v_mois
                );
            SET v_mois = v_mois+1;
        END WHILE;   
        SET v_year = v_year-1;
        SET c_year = c_year+1;
     END WHILE;   
END