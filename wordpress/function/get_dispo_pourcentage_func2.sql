CREATE DEFINER=`sa`@`%` FUNCTION `get_dispo_pourcentage_func2`(
id_desti int(11),
id_heberg int(11),
id_peri int(11)
) RETURNS int(11)
BEGIN
    declare dispo int(11) default 0;

    SELECT (SELECT 
        ROUND((SELECT 
            (SELECT qte FROM vac_association_cata2 vac
                WHERE vac.id_periode=rc.id_periode AND vac.id_hebergement=rc.id_hebergement AND vac.id_destination=r.id_destination)
            - sum(rc.qte)) *100 /
            (SELECT qte FROM vac_association_cata2 vac
              WHERE vac.id_periode=rc.id_periode AND vac.id_hebergement=rc.id_hebergement AND vac.id_destination=r.id_destination))
    ) as pourcent into dispo
    FROM vac_resa2 r
    INNER JOIN vac_resa_conf2 rc ON rc.stece=r.stece AND rc.dossier=r.dossier
    WHERE r.id_destination=id_desti and rc.id_hebergement=id_heberg and rc.id_periode=id_peri
    GROUP BY rc.id_periode, rc.id_hebergement, r.id_destination;
RETURN dispo;
END