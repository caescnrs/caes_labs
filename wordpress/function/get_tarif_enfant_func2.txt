CREATE FUNCTION  `get_tarif_enfant_func2`(
    id_asso int(11),
    ext bit(1),
    unite varchar(5),
    tranche_inf smallint(6),
    tranche_sup smallint(6)
) RETURNS decimal(6,2)
BEGIN
    DECLARE tarif_enfant2 DECIMAL(6,2);
    
    SELECT distinct(ate.tx_reduction) into tarif_enfant2 
    FROM `vac_association_tenf2` ate 
    INNER JOIN `vac_tarifs_enfants2` te ON te.id=ate.id_tarif_enfant 
    WHERE ate.id_association = id_asso and te.exterieur = ext and te.unite = unite and te.tranche_inferieure = tranche_inf and te.tranche_superieure = tranche_sup;
RETURN tarif_enfant2;
END