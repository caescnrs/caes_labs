CREATE DEFINER=`sa`@`%` FUNCTION `get_web_reservation_func`(
    pid_association int(11),
    pdate date
) RETURNS int(11)
BEGIN
    declare qte int(11);
    
 

    select IF(h.id = 25, count(p.id), count(w.id)) into qte from vac_association_cata2 a 
    INNER JOIN web_reservation w ON w.id_destination = a.id_destination and w.type_hebergement = a.id_hebergement and w.etat = 9 and w.date_debut <= pdate and pdate <= w.date_fin
    INNER JOIN p_hebergement2 h on h.id= w.type_hebergement
    INNER JOIN web_reservation_participant p on p.id_reservation = w.id
    where a.id = pid_association;
    
    if isnull(qte) then
        set qte = 0;
    end if;
RETURN qte;
END