CREATE DEFINER=`sa`@`` FUNCTION `wp_get_attribution_func`(
    pid_association int(11),
    pdate date
) RETURNS int(11)
BEGIN
    declare qte int(11);
    
    select sum(a.qte) into qte from vac_attribution a where a.id_association=pid_association
    and a.date_debut <= pdate and pdate <= a.date_fin;
    
    if isnull(qte) then
        set qte = 0;
    end if;
RETURN qte;
END