USE `ganaelbd`;
DROP function IF EXISTS `wp_get_dispo_func`;

DELIMITER $$
USE `ganaelbd`$$
CREATE DEFINER=`sa`@`` FUNCTION `wp_get_dispo_func`(
    pid_catalogue int(11),
    pid_type_destination int(11),
    pid_destination int(11),
    pid_periode int(11),
    pid_hebergement int(11),
    pdate date
) RETURNS int(11)
BEGIN
    declare qte int(11);
    
    select sum(c.qte) into qte from `vac_resa2` r inner join `vac_resa_conf2` c on c.stece = r.stece and c.dossier = r.dossier
    where r.id_catalogue = pid_catalogue and r.id_type_destination = pid_type_destination and r.id_destination = pid_destination
    and c.id_periode = pid_periode and c.id_hebergement = pid_hebergement
    and r.date_deb_resa <= pdate and pdate <= r.date_fin_resa;
    
    if isnull(qte) then
        set qte = 0;
    end if;
RETURN qte;
END$$

DELIMITER ;

