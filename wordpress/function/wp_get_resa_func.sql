CREATE DEFINER=`sa`@`` FUNCTION `wp_get_resa_func`(
    pid_catalogue int(11),
    pid_type_destination int(11),
    pid_destination int(11),
    pid_hebergement int(11),
    pdate date
) RETURNS int(11)
BEGIN
    declare pqte int(11) default 0;
    
    select sum(c.qte) into pqte from `vac_resa2` r inner join `vac_resa_conf2` c
    on c.stece = r.stece and c.dossier = r.dossier
    where r.id_catalogue = pid_catalogue and r.id_type_destination = pid_type_destination
    and r.id_destination = pid_destination and c.id_hebergement = pid_hebergement
    and pdate between r.date_deb_resa and r.date_fin_resa;
    
    if isnull(pqte) then
        set pqte= 0;
    end if;
RETURN pqte;
END