﻿<?php
$root = realpath($_SERVER["DOCUMENT_ROOT"]);
include "connect_caesdb.php";

$saison = get_field('selection_saison', 'option');
$ville = get_field( 'sejours_fournisseurs' );
$catalogue_hiver = get_field('libelle_hiver', 'option');
$catalogue_ete = get_field('libelle_ete', 'option');
$catalogue = '18';
$date_debut = date('d-m-Y',time());
if (isset($_POST['date_debut']) )
{
    $date_debut = $_POST['date_debut'];
}
$date_fin = '31-12-' . date('Y');
if (isset($_POST['date_fin']) )
{
    $date_fin = $_POST['date_fin'];
}
$nombre_participant = 0;
if (isset($_POST['nombre_participant']) )
{
    $nombre_participant = $_POST['nombre_participant'];
}
// Affichages Tarifs Personnalisés
if (get_field('c_sef_tarifs_personnalises')) {
	the_field('c_sef_tarifs_personnalises');
} else {
		$date_debut_eng = date('Y-m-d', strtotime(str_replace('/', '-', $date_debut)));
		$date_fin_eng = date('Y-m-d', strtotime(str_replace('/', '-', $date_fin)));
		$destinations_semaine = $mydb->get_results("CALL sp_wp_get_destination2('$catalogue','$ville','SEMAINE', '$date_debut_eng', '$date_fin_eng','$nombre_participant')"); 
		$destinations_journee = $mydb->get_results("CALL sp_wp_get_destination2('$catalogue','$ville','JOURNEE', '$date_debut_eng', '$date_fin_eng','$nombre_participant')"); 
		$destinations_periode = $mydb->get_results("CALL sp_wp_get_destination2('$catalogue','$ville','PERIODE', '$date_debut_eng', '$date_fin_eng','$nombre_participant')"); ?>
<!--
	<form action="#tarifs" method="post">
		<div style="block; border:1px solid #ccc; padding:20px; margin-bottom:20px;border-radius:10px; line-height:20px; font-size:14px">
			<center> <h4> Affiner ma recherche </h4></center>
			
			<p>Début : <input type="text" name="date_debut" value="<?php echo $date_debut; ?>"/></p>
			<p>Fin : <input type="text" name="date_fin" value="<?php echo $date_fin; ?>"/></p>
			<p>Nombre de participants : <input type="text" name="nombre_participant" value="<?php echo $nombre_participant; ?>"/></p>
			<p><input type="submit" value="OK"></p>
		</div>		
	</form>
-->
	<?php 
	 if 	(!empty($destinations_semaine) or !empty($destinations_journee) or !empty($destinations_periode)){?>
<?php if (has_term(  'les-villages-du-caes', 'types_de_sejours' )) { ?>
	<div style="block; border:1px solid #ccc; padding:20px; margin-bottom:20px;border-radius:10px; line-height:20px; font-size:14px">
		<div style="width:50px;height:20px;background:#ddddf1; float:left; margin-right:10px;">&nbsp;</div>Tarifs soumis à sélection - <a href='#' data-toggle='modal' data-target='#regle_resa' style='color:inherit'><i class='fa fa-search' aria-hidden='true'></i>
 			En savoir plus</a>
	</div>
	<?php } ?>
	<?php
		$catalogue = $catalogue;
		$semaine = 'SEMAINE'; 			
		$href = 'type1';
		include ('requetes_sejours.php'); 
	}			
}?>
<script>
$(document).ready(function() {	
	$('#debut').datepicker({
			autoclose: true,
			language: 'fr'
	}).on('changeDate', function(e) {
		$('#form').bootstrapValidator('revalidateField', 'debut');
	});
});
</script>