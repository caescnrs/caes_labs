<?php
$rowspan = 1;
$colspan=0;
$libelle_tarif = '';
if ($type_hebergement->tx_reduction>0){
    if ($type_hebergement->px_sanspens>0 or $type_hebergement->px_demi_pension>0 or $type_hebergement->px_pension_cplete>0)$rowspan = 2 ;
        if ($type_hebergement->px_sanspens>0)$colspan++;
        if ($type_hebergement->px_demi_pension>0)$colspan++;
        if ($type_hebergement->px_pension_cplete>0)$colspan++;
};
    echo "<table>        
        <tr>";			
			
			if ($destination->offre=='N') $libelle_tarif .= ' à la SEMAINE';	
			else $libelle_tarif .= ' à la PERIODE';	
			echo "<th rowspan='$rowspan'>Tarif $libelle_tarif</th>";		
            if ($type_hebergement->logement==1){
                echo "
                    <th rowspan='$rowspan'>Location</th>
                        ";
            }
            if ($type_hebergement->logement<>1){
                if ($type_hebergement->px_demi_pension>0) {
                    echo "<th rowspan='$rowspan'>Demi Pension</th>";
                }
                if ($type_hebergement->px_pension_cplete>0) {
                    echo "<th rowspan='$rowspan'>Pension Complète</th>";
                }
            }
            else if ($type_hebergement->px_sanspens>0 or $type_hebergement->px_demi_pension>0 or $type_hebergement->px_pension_cplete>0){
                echo "<th colspan='$colspan'>Suppléments par personne</th>";	
            }
            if (($type_hebergement->px_demi_pension>0 or $type_hebergement->px_pension_cplete>0) and $type_hebergement->tx_reduction>0){
                echo "
                    <th colspan='4' style='background:#ccc'>Réductions enfants</th>
                        ";
                    };	
            echo "		
                <th rowspan='$rowspan'>Extérieur</th>	
            ";
            echo "		
                <th rowspan='$rowspan'>Dispo</th>	
            ";
    echo " </tr>";
    if (($type_hebergement->px_sanspens>0 or $type_hebergement->px_demi_pension>0 or $type_hebergement->px_pension_cplete>0) and $type_hebergement->tx_reduction>0){
        echo "<tr>";				
            if ($type_hebergement->logement==1){
                if ($type_hebergement->px_sanspens>0) {
                    echo "<th>Sans Pension</th>";
                }
                if ($type_hebergement->px_demi_pension>0) {
                    echo "<th>Demi Pension</th>";
                }
                if ($type_hebergement->px_pension_cplete>0) {
                    echo "<th>Pension Complète</th>";
                }
            }			
            echo "
            <th style='background:#ccc'>14 à -18 ans</th>
                <th style='background:#ccc'>8 à -14 ans</th>
                <th style='background:#ccc'>3 à -8 ans</th>	
                <th style='background:#ccc'>3 mois à -3 ans</th>
                ";
            echo "</tr>";
    }
    
$ressources = $mydb->get_results("CALL sp_wp_get_vac_ressources2('$destination->destination','$catalogue','$type_hebergement->type_hebergement', '$semaine', '$date_debut_eng', '$date_fin_eng')");	
foreach ($ressources as $ressource) {
    $url_reservation = "http://sv-webapptest.caes.cnrs.fr:6542/reservation_ajout_wp/".$destination->destination."/".$ressource->periode."/".$type_hebergement->type_hebergement;
    $pourcentage_dispo = $ressource->dispo;
    
    if (76 < $pourcentage_dispo && $pourcentage_dispo <= 100){
        $class_occupation = '#95C11F';
    }
    elseif 	($pourcentage_dispo < 1){
        $class_occupation = '#E30613';
    }
    elseif 	(1 < $pourcentage_dispo && $pourcentage_dispo < 26){
        $class_occupation = '#F39200';
    }
    elseif 	(26 < $pourcentage_dispo  && $pourcentage_dispo < 51){
        $class_occupation = '#FFD500';
    }
    elseif (51 < $pourcentage_dispo && $pourcentage_dispo < 76){
        $class_occupation = '#BCCF00';
    }
    
    if ($pourcentage_dispo < 1)
        $text_percent = 'complet';
    else 
        $text_percent = $pourcentage_dispo."%";
	

    if 	($ressource->selection == 1){
        echo "<tr  style='background-color: #ddddf1;' >";
            echo "<td>du ".$ressource->debut. " au " .$ressource->fin." <br />
            <a href='#' data-toggle='modal' data-target='#regle_resa' style='color:inherit'><i class='fa fa-search' aria-hidden='true'></i>
            En savoir plus</a>
                </td>";
    }
    else {
        echo "<tr>";
            echo "<td> du ".$ressource->debut. " au " .$ressource->fin."</td>";
    }
    if ($type_hebergement->logement==1)	echo "<td>".$ressource->px_don_dro." €</td>";
    if ($type_hebergement->px_sanspens>0)echo "<td>".$ressource->px_sanspens_f." €</td>";
    if ($type_hebergement->px_demi_pension>0)echo "<td>".$ressource->px_demi_pension_f." €</td>";
    if ($type_hebergement->px_pension_cplete>0){
		if ($ressource->px_pension_cplete_f>0) echo "<td>".$ressource->px_pension_cplete_f." €</td>"; else echo "<td> - </td>";
	}
    if (($type_hebergement->px_demi_pension>0 or $type_hebergement->px_pension_cplete>0) and $type_hebergement->tx_reduction>0){
		if(!$ressource->tarif_enfant_14_18_f) echo "<td></td>"; else echo "<td>".$ressource->tarif_enfant_14_18_f." %</td>";
		if(!$ressource->tarif_enfant_8_14_f) echo "<td></td>"; else echo "<td>".$ressource->tarif_enfant_8_14_f." %</td>";
		if(!$ressource->tarif_enfant_3_8_f) echo "<td></td>"; else echo "<td>".$ressource->tarif_enfant_3_8_f." %</td>";
		if($ressource->tarif_enfant_3_3_f==100) echo "<td>GRATUIT</td>"; else if (!$ressource->tarif_enfant_3_3_f) echo "<td></td>"; else echo "<td>".$ressource->tarif_enfant_3_3_f." %</td>";
    }
    $href_tarif_exterieur = $ressource->periode.$ressource->ressource;
    echo "<td>
        <a href='#' data-toggle='modal' target='_blank' data-target=#".$href_tarif_exterieur." style='color:inherit'><i class='fa fa-search' aria-hidden='true'></i>
            Afficher
        </a>
        <div class='modal fade bannerformmodal' tabindex='-1' role='dialog' aria-labelledby='bannerformmodal' aria-hidden='true' id=".$href_tarif_exterieur.">
            <div class='modal-dialog modal-sm'>
                <div class='modal-content'>
                    <div class='modal-content'>
                        <div class='modal-header'>
                            <button type='button' class='close' data-dismiss='modal' aria-hidden='true'>&times;</button>
                                <h4 class='modal-title' id='myModalLabel'>Tarifs pour extérieur</h4>
                        </div>
                        <div class='modal-body'>
                        <table>
                            <tr>";
                                if ($type_hebergement->logement==1){
                                    echo "
                                        <th>Tarif à la $semaine</th>
                                            ";		
                                }
                                else echo "<th>Tarif à la $semaine par personne</th>";	
                                if ($type_hebergement->logement==1){
                                    echo "
                                        <th>Location</th>
                                            ";
                                }
                                if ($ressource->px_sanspens_e>0) echo "<th>Sans pension</th>";
                                if ($ressource->px_demi_pension_e>0) echo "<th>Demi-Pension</th>";
                                if ($ressource->px_pension_cplete_e>0) echo "<th>Pension-Complète</th>";
                                if (($type_hebergement->px_demi_pension>0 or $type_hebergement->px_pension_cplete>0) and $type_hebergement->tx_reduction>0){
                                    echo "
                                        <th>14 à -18 ans</th>
                                            <th>8 à -14 ans</th>
                                            <th>3 à -8 ans</th>	
                                            <th>3 mois à -3 ans</th>					
                                            ";
                                }
                                ?>
                                </tr>
                                <tr>
                                <?php
                                    echo "<td>du ".$ressource->debut. " au " .$ressource->fin." <br />";
                                    if ($type_hebergement->logement==1){
                                        echo "
                                            <td>".$ressource->px_exterieur." €</td>
                                                ";
                                    }
                                    if ($ressource->px_sanspens_e>0) echo "<td>".$ressource->px_sanspens_e." €</td>";
                                    if ($ressource->px_demi_pension_e>0) echo "<td>".$ressource->px_demi_pension_e." €</td>";
                                    if ($ressource->px_pension_cplete_e>0) echo "<td>".$ressource->px_pension_cplete_e." €</td>";
                                    if (($type_hebergement->px_demi_pension>0 or $type_hebergement->px_pension_cplete>0) and $type_hebergement->tx_reduction>0){
										if(!$ressource->tarif_enfant_14_18_e) echo "<td></td>"; else echo "<td>".$ressource->tarif_enfant_14_18_e." %</td>";
										if(!$ressource->tarif_enfant_8_14_e) echo "<td></td>"; else echo "<td>".$ressource->tarif_enfant_8_14_e." %</td>";
										if(!$ressource->tarif_enfant_3_8_e) echo "<td></td>"; else echo "<td>".$ressource->tarif_enfant_3_8_e." %</td>";
										if($ressource->tarif_enfant_3_3_e==100) echo "<td>GRATUIT</td>"; else if (!$ressource->tarif_enfant_3_3_e) echo "<td></td>"; else echo "<td>".$ressource->tarif_enfant_3_3_e." %</td>";
									}
                                    ?>
                                    </tr>
                                </table>
                            </div>
                        <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Fermer</button>
                        </div>
                    </div>
                </div> 
            </div>
        </div>
    </td>
    <?php
    $dispo = $destination->destination.$type_hebergement->type_hebergement.$ressource->periode;
    $dispos = $mydb->get_results("CALL sp_wp_get_dispo('$destination->destination','$ressource->periode','$type_hebergement->type_hebergement', NULL, NULL)");
	$pourcent_periode = 0;
	$total_org = 0;
	$total_resa =0;
	$total_attribution =0;
	$color_periode = '#95C11F';
	foreach ($dispos as $item){
		$total_org += $item->qte_org;
		$total_resa += $item->qte_resa;
		$total_attribution += $item->qte_attribution;
	};
	if ($total_org == 0) $total_org = 1;
	$pourcent_periode = round(100-(($total_resa+$total_attribution)/$total_org)*100);
	if 	($pourcent_periode < 1){
        $color_periode = '#E30613';
    }
    elseif 	(0 < $pourcent_periode && $pourcent_periode < 26){
        $color_periode = '#F39200';
    }
    elseif 	(26 < $pourcent_periode  && $pourcent_periode < 51){
        $color_periode = '#FFD500';
    }
    elseif (51 < $pourcent_periode && $pourcent_periode < 76){
        $color_periode = '#BCCF00';
    }
if ($destination->offre =='N'){    
echo "<td style='background-color:$color_periode;color:#fff;text-align:center;font-size:11px;font-weight:bold;padding-left:5px; padding-right:5px;'>
        <a href='#' data-toggle='modal' data-target=#".$dispo." style='color:inherit'><i class='fa fa-search' aria-hidden='true'></i>
            Afficher
        </a>
        <div class='modal fade bannerformmodal' tabindex='-1' role='dialog' aria-labelledby='bannerformmodal' aria-hidden='true' id=".$dispo.">
            <div class='modal-dialog modal-sm'>
                <div class='modal-content'>
                    <div class='modal-content'>
                        <div class='modal-header'>
                            <button type='button' class='close' data-dismiss='modal' aria-hidden='true'>&times;</button>
                                <h3>$destination->libelle ( $type_hebergement->libelle )</h3>
                                <h4>Disponibilités sur la période du $ressource->debut au $ressource->fin</h4>
                        </div>
                        <div class='modal-body'>
							<p style='color:black;'>
								Disponibilités:
								<span style='background-color: #BCCF00'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span> 75 à 100%
								<span style='background-color: #95C11F'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span> 50 à 75%
								<span style='background-color: #FFD500'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span> 25 à 50%
								<span style='background-color: #F39200'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span> 1 à 25%
								<span style='background-color: #E30613'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span> Complet
							</p>
                            <table>
                                <tr>
                                    <th>Dimanche</th>
                                    <th>Lundi</th>
                                    <th>Mardi</th>
                                    <th>Mercredi</th>
                                    <th>Jeudi</th>
                                    <th>Vendredi</th>
                                    <th>Samedi</th>
                                </tr>";
                                $i = 0;
                                foreach ($dispos as $dispo) {
		$pourcent_dispo_day = round(100-(($dispo->qte_resa+$dispo->qte_attribution)/$dispo->qte_org)*100);
    
    if (76 < $pourcent_dispo_day && $pourcent_dispo_day <= 100){
        $class_occupation = '#95C11F';
    }
    elseif 	($pourcent_dispo_day < 1){
        $class_occupation = '#E30613';
    }
    elseif 	(0 < $pourcent_dispo_day && $pourcent_dispo_day < 26){
        $class_occupation = '#F39200';
    }
    elseif 	(26 < $pourcent_dispo_day  && $pourcent_dispo_day < 51){
        $class_occupation = '#FFD500';
    }
    elseif (51 < $pourcent_dispo_day && $pourcent_dispo_day < 76){
        $class_occupation = '#BCCF00';
    }
									if ($i == 0 and $dispo->day <> 1) echo "<tr>";
									if ($i == 0){
										for($j=2; $j<=$dispo->day; $j++){
											echo "<td></td>";
										}									
									}
                                    if ($dispo->day == 1) echo "<tr>";
	echo "<td style='background-color:$class_occupation;color:#fff;text-align:center;font-size:11px;font-weight:bold;padding-left:5px; padding-right:5px;'>".$dispo->db_date_fr."</td>";
                                    if ($dispo->day == 7) echo "</tr>";
                                    $i++;
                                };
                                if ($dispo->day <> 7) echo "</tr>";
                                echo"
                            </table>
                        </div>
                        <div class='modal-footer'>
                        <button type='button' class='btn btn-default' data-dismiss='modal'>Fermer</button>
                        </div>
                    </div>
                </div> 
            </div>
        </div>
    </td>
  </tr>";
}
else {
echo "<td style='background-color:$color_periode;color:#fff;text-align:center;font-size:11px;font-weight:bold;padding-left:5px; padding-right:5px;'>$pourcent_periode %</td></tr>";
}
  
}
echo "</table>";	
echo "
	<div class='modal fade bannerformmodal' tabindex='-1' role='dialog' aria-labelledby='bannerformmodal' aria-hidden='true' id='regle_resa'>
		<div class='modal-dialog modal-sm'>
			<div class='modal-content'>
				<div class='modal-content'>
					<div class='modal-header'>
						<button type='button' class='close' data-dismiss='modal' aria-hidden='true'>&times;</button>
						<h4 class='modal-title' id='myModalLabel'>Règles de Réservation</h4>
					</div>
					<div class='modal-body'>";
						the_field('c_sef_regles_reservation');
					echo "</div>
					<div class='modal-footer'>
						<button type='button' class='btn btn-default' data-dismiss='modal'>Fermer</button>
					</div>
				</div>
			</div> 
		</div>
	</div>
";
include ('requete_sejours_journee.php'); 
include ('requete_sejours_dispo.php'); 
?>




