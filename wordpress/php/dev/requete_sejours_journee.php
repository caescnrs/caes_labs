<?php
echo "
	<div class='modal fade bannerformmodal' tabindex='-1' role='dialog' aria-labelledby='bannerformmodal' aria-hidden='true' id=".$href_tarif_journee.">
		<div class='modal-dialog modal-sm'>
			<div class='modal-content'>
                <div class='modal-header'>
                    <button type='button' class='close' data-dismiss='modal' aria-hidden='true'>&times;</button>
                        <h3>$destination->libelle</h3>
                        <h4>$type_hebergement->libelle</h4>
                </div>
                    <div class='modal-body'>
";
$rowspan = 1;
$colspan=0;
if ($type_hebergement->tx_reduction>0){
	if ($type_hebergement->px_sanspens>0 or $type_hebergement->px_demi_pension>0 or $type_hebergement->px_pension_cplete>0)$rowspan = 2 ;
		if ($type_hebergement->px_sanspens>0)$colspan++;
		if ($type_hebergement->px_demi_pension>0)$colspan++;
		if ($type_hebergement->px_pension_cplete>0)$colspan++;
};
	echo "<table>        
		<tr>";
			if ($type_hebergement->logement==1){
				echo "
					<th rowspan='$rowspan'>Tarif à la Journée</th>
						";		
			}
			else echo "
				<th rowspan='$rowspan'>Tarif à la Journée par personne</th>
				";	
			if ($type_hebergement->logement==1){
				echo "
					<th rowspan='$rowspan'>Location</th>
						";
			}
			if ($type_hebergement->logement<>1){
				if ($type_hebergement->px_demi_pension>0) {
					echo "<th rowspan='$rowspan'>Demi Pension</th>";
				}
				if ($type_hebergement->px_pension_cplete>0) {
					echo "<th rowspan='$rowspan'>Pension Complète</th>";
				}
			}
			else if ($type_hebergement->px_sanspens>0 or $type_hebergement->px_demi_pension>0 or $type_hebergement->px_pension_cplete>0){
				echo "<th colspan='$colspan'>Suppléments par personne</th>";	
			}
            if (($type_hebergement->px_demi_pension>0 or $type_hebergement->px_pension_cplete>0) and $type_hebergement->tx_reduction>0){
                echo "
                    <th colspan='4' style='background:#ccc'>Réductions enfants</th>
					";
                    
			};	
	echo " </tr>";
	if (($type_hebergement->px_sanspens>0 or $type_hebergement->px_demi_pension>0 or $type_hebergement->px_pension_cplete>0) and $type_hebergement->tx_reduction>0){
		echo "<tr>";				
			if ($type_hebergement->logement==1){
				if ($type_hebergement->px_sanspens>0) {
					echo "<th>Sans Pension</th>";
				}
				if ($type_hebergement->px_demi_pension>0) {
					echo "<th>Demi Pension</th>";
				}
				if ($type_hebergement->px_pension_cplete>0) {
					echo "<th>Pension Complète</th>";
				}
			}			
            echo "
            <th style='background:#ccc'>14 à -18 ans</th>
                <th style='background:#ccc'>8 à -14 ans</th>
                <th style='background:#ccc'>3 à -8 ans</th>	
                <th style='background:#ccc'>3 mois à -3 ans</th>
                ";
            echo "</tr>";
	}
	
$ressources = $mydb->get_results("CALL sp_wp_get_vac_ressources2('$destination->destination','$catalogue','$type_hebergement->type_hebergement', '$semaine', '$date_debut_eng', '$date_fin_eng')");	
foreach ($ressources as $ressource) {

	if 	($ressource->selection == 0){
		echo "<tr>";
			echo "<td> du ".$ressource->debut. " au " .$ressource->fin."</td>";
		if ($type_hebergement->logement==1){	echo "<td>".$ressource->px_don_dro_j." €</td>";}
		if ($type_hebergement->px_sanspens>0)echo "<td>".$ressource->px_sanspens_f_j." €</td>";
		if ($type_hebergement->px_demi_pension>0)echo "<td>".$ressource->px_demi_pension_f_j." €</td>";
		if ($type_hebergement->px_pension_cplete>0)echo "<td>".$ressource->px_pension_cplete_f_j." €</td>";
		if (($type_hebergement->px_demi_pension>0 or $type_hebergement->px_pension_cplete>0) and $type_hebergement->tx_reduction>0){
			echo "<td>".$ressource->tarif_enfant_14_18_f." %</td>";
				echo "<td>".$ressource->tarif_enfant_8_14_f." %</td>";
				echo "<td>".$ressource->tarif_enfant_3_8_f." %</td>";
				if ($ressource->tarif_enfant_3_3_f==100) echo "<td>GRATUIT</td>";
				else echo "<td>".$ressource->tarif_enfant_3_3_f." %</td>";
		};
	echo "</tr>";    
	}
	
}
echo "</table>




				</div>
				</div>
			</div> 
	</div>
";
?>
