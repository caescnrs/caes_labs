<?php 
$destinations = $mydb->get_results("CALL sp_wp_get_destination2('$catalogue','$ville','$semaine', '$date_debut_eng', '$date_fin_eng','$nombre_participant')");

?>


<?php 
if 	(!empty($destinations)){
	?>
	<?php foreach ($destinations as $destination){   					
		?>
		<div style=" margin-bottom:20px;text-align:center;border:1px solid #ddd">
			<h3 class="panel-title" style="padding-top:10px;">
				<?php echo "$destination->libelle "?>
			</h3>

			<?php $type_hebergements = $mydb->get_results("CALL sp_wp_get_vac_ressource_all2('$destination->destination', '$semaine')");
			$href_tarif_jounee = "J";
			foreach ($type_hebergements as $type_hebergement) {
				?>
				<?php 
					if ($destination->type_destination == 'Centres partenaires' or $destination->type_destination == 'Centres extérieurs'){
						echo "<h4 style='padding-top:10px; padding-bottom:10px;'>".$type_hebergement->libelle."</h4>"; 
						include ('requete_sejours_partenaire.php'); 
					 }
					else{
						$href_tarif_journee = $destination->destination.$type_hebergement->type_hebergement;
						if ($destination->offre=='N' and $type_hebergement->annee == '2019'){
							echo "
							<h3 style='padding-top:10px; padding-bottom:10px;'>".$type_hebergement->libelle." ";
								if(~ $type_hebergement->hautesaison){
									echo "<button onclick=\"window.open('https://moncompte.caes.cnrs.fr/reservation_demande/".$destination->destination."/".$type_hebergement->type_hebergement ."', '_blank');\"> Réserver </button>";
								}
							echo "</h3>
							<a href='#' data-toggle='modal' data-target=#".$href_tarif_journee."   style='float: right;width:100%;margin-bottom:10px;' >
								<i class='fa fa-search' aria-hidden='true'></i>Consulter les tarifs à la journée
							</a>
							
							"; 
						
						}
						else{
							echo "
							<h4 style='padding-top:10px; padding-bottom:10px;'>".$type_hebergement->libelle."</h4>
							"; 
						}
						include ('requete_sejours_caes.php'); 
					 }

			};	
			if ($destination->info_complementaire)
				echo "<p class='condition_prix'><br /><strong>Informations complétementaires : </strong> $destination->info_complementaire</p>";
			if ($destination->info_compris)
				echo "<p class='condition_prix'><br /><strong>Ces prix comprennent : </strong> $destination->info_compris</p>";
			if ($destination->info_pas_compris)
				echo "<p class='condition_prix'><br /><strong>Ces prix ne comprennent pas :</strong>  $destination->info_pas_compris</p>";
			?>
			<br />
			
		</div>
	<?php };
};
?>