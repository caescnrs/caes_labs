<?php
/*
Template Name: Page Correspondace Séjours
*/
?>
<?php get_header();
$args = array(
    'post_type' => 'sejours',
    'posts_per_page' => -1,
    'orderby' => 'meta_value',
    'meta_key' => 'sejours_fournisseurs',
	'order'   => 'ASC',
    'tax_query' => array(
    array(
        'taxonomy'  => 'types_de_sejours',
        'field'     => 'slug',
        'terms'     => 'les-sejours-jeunes', // exclude items media items in the news-cat custom taxonomy
        'operator'  => 'NOT IN'),
    
    array(
        'taxonomy'  => 'types_de_sejours',
        'field'     => 'slug',
        'terms'     => 'les-sejours-a-theme', // exclude items media items in the news-cat custom taxonomy
        'operator'  => 'NOT IN')

        ),
);

$my_query = new WP_Query($args); ?>


    <div class="<?php echo vct_get_content_container_class(); ?>">
        <div class="content-wrapper">
            <div class="row">
                <div class="<?php echo vct_get_maincontent_block_class(); ?>">
                    <div class="main-content">
                        
                        <div class="col-md-6" style="text-transform:uppercase;">
                        <?php
                        if($my_query->have_posts()) : while ($my_query->have_posts() ) : $my_query->the_post();
                            
                            if (get_field('sejours_fournisseurs')) {
                            echo'<p style="color:green">';
                            the_field('sejours_fournisseurs');
                            } else {
                                echo'<p style="color:red">';echo 'Vide';
                            }
                            echo ' <--> ';
                            the_title();
                            echo '</p>';

                        endwhile;
                        endif;
                        wp_reset_postdata();
                        ?>
                        </div>
                        
                        <div id="table-ganael" class="col-md-6">
                            <?php include ('template_requetes_sejours/requete_sejours_correspondance.php');?>
                            
                        </div>
                    </div><!--.main-content-->
                </div><!--.<?php echo vct_get_maincontent_block_class(); ?>-->

                <?php if ( vct_get_sidebar_class() ): ?>
                    <?php get_sidebar(); ?>
                <?php endif; ?>

            </div><!--.row-->
        </div><!--.content-wrapper-->
    </div><!--.<?php echo vct_get_content_container_class(); ?>-->
<?php get_footer();
