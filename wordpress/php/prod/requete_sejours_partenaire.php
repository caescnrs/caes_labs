<?php 
echo "<table>        
		<tr>";
			if ($type_hebergement->logement==1){
				echo "
					<th>Tarif à la $semaine</th>
				";		
			}
			else echo "<th>Tarif à la $semaine par personne</th>";	
			if ($type_hebergement->logement==1){
				echo "
				   <th>Location</th>
				";
			}
			if ($type_hebergement->px_demi_pension>0) {
				if ($type_hebergement->logement==1) echo "<th>Demi Pension par personne</th>";
				else echo "<th>Demi Pension</th>";
			}
			if ($type_hebergement->px_pension_cplete>0) {
				if ($type_hebergement->logement==1) echo "<th>Pension Complète par personne</th>";
				else echo "<th>Pension Complète</th>";
			}
			if ($type_hebergement->px_demi_pension>0 or $type_hebergement->px_pension_cplete>0) echo "<th>Enfants</th>";
echo " </tr>";

$ressources = $mydb->get_results("CALL sp_wp_get_vac_ressources2('$destination->destination','$catalogue','$type_hebergement->type_hebergement', '$semaine', '$date_debut_eng', '$date_fin_eng')");	

foreach ($ressources as $ressource) {
		$tarifs_enfants = $mydb->get_results("CALL sp_wp_get_tarifs_enfants_partenaires2('$ressource->id_association')");					
		echo "<tr>";
			echo "<td> du ".$ressource->debut. " au " .$ressource->fin."</td>";
			if ($type_hebergement->logement==1){	echo "<td>".$ressource->px_don_dro." €</td>";}
			if ($type_hebergement->px_demi_pension>0){
				if ($ressource->px_demi_pension_f>0){
					echo "<td>".$ressource->px_demi_pension_f." €</td>";
				}
				else{
					echo "<td> NON DISPONIBLE </td>";
				}
			}
			if ($type_hebergement->px_pension_cplete>0){
				if ($ressource->px_pension_cplete_f>0){
					echo "<td>".$ressource->px_pension_cplete_f." €</td>";
				}
				else{
					echo "<td> NON DISPONIBLE </td>";
				}
			}
		if ($type_hebergement->px_demi_pension>0 or $type_hebergement->px_pension_cplete>0){
			$href_tarif_exterieur = $ressource->periode.$ressource->ressource;		
			echo "<td><a href='#' data-toggle='modal' data-target=#".$href_tarif_exterieur." style='color:inherit'><i class='fa fa-search' aria-hidden='true'></i>
				Afficher</a>
				<div class='modal fade bannerformmodal' tabindex='-1' role='dialog' aria-labelledby='bannerformmodal' aria-hidden='true' id=".$href_tarif_exterieur.">
					<div class='modal-dialog modal-sm'>
						<div class='modal-content'>
							<div class='modal-content'>
								<div class='modal-header'>
									<button type='button' class='close' data-dismiss='modal' aria-hidden='true'>&times;</button>
									<h4 class='modal-title' id='myModalLabel'>Tarifs pour enfants</h4>
								</div>
								<div class='modal-body'>
									<table>
										<tr>
											<th>Catégorie</th>";
											if ($type_hebergement->px_demi_pension>0) echo "<th>Demi Pension</th>";
											if ($type_hebergement->px_pension_cplete>0) echo "<th>Pension Complète</th>";
										echo "</tr>";
										foreach ($tarifs_enfants as $tarifs_enfant) {	
											echo "<tr>";									
												echo "<td>".$tarifs_enfant->libelle."</td>";			
												if ($type_hebergement->px_demi_pension>0)	{				
												if ($tarifs_enfant->px_demi_pension==0) echo "<td>GRATUIT</td>";
												else echo "<td>".$tarifs_enfant->px_demi_pension." €</td>";		
												}												
												if ($type_hebergement->px_pension_cplete>0)	{											
												if ($tarifs_enfant->px_pension_cplete==0) echo "<td>GRATUIT</td>";
												else echo "<td>".$tarifs_enfant->px_pension_cplete." €</td>";	
												}												
											echo"</tr>";	
											}
											?>
										</tr>
									</table>
									
								</div>
								<div class="modal-footer">
									<button type="button" class="btn btn-default" data-dismiss="modal">Fermer</button>
								</div>

							</div>
						</div> 
					</div>
				</div>
			</td>
		<?php } ?>
	</tr>  
  <?php
}
echo "</table>";
?>