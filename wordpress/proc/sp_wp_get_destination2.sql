CREATE DEFINER=`sa`@`%` PROCEDURE `sp_wp_get_destination2`(p_catalogue VARCHAR(10), p_wp_destination VARCHAR(50), p_semaine VARCHAR(10) , p_periode_debut DATE, p_periode_fin DATE, p_nombre_particpant INT(10))
BEGIN
    SELECT distinct(d.id) as destination, d.libelle, d.info_complementaire, d.info_compris, d.info_pas_compris, d.offre,
    td.libelle as type_destination
    FROM vac_destination2 d
    INNER JOIN vac_association_cata2 ac ON ac.id_destination=d.id and ac.aff_web=1
    INNER JOIN vac_periodes2 p ON p.id=ac.id_periode
    INNER JOIN vac_type_destination2 td ON td.id=ac.id_type_destination
    WHERE d.libelle LIKE CONCAT('%', p_wp_destination , '%') AND d.aff_web=1 AND NOW()<p.date_fin
    GROUP BY d.id
    ORDER BY p.date_debut;
END