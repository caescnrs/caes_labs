USE `ganaelbd`;
DROP procedure IF EXISTS `sp_wp_get_dispo`;

DELIMITER $$
USE `ganaelbd`$$
CREATE DEFINER=`sa`@`` PROCEDURE `sp_wp_get_dispo`(
    pid_destination int(11),
    pid_periode int(11),
    pid_hebergement int(11),
    pdate_debut date,
    pdate_fin date
)
BEGIN
    if pid_periode = 0 and pid_hebergement = 0 then 
        select t.db_date, a.id_catalogue, a.id_type_destination, a.id_destination, a.id_periode, a.id_hebergement,
        date_format(t.db_date, '%d/%m/%Y') as db_date_fr, dayofweek(t.db_date) as day, a.qte as qte_org,
        `wp_get_dispo_func`(a.id_catalogue, a.id_type_destination, a.id_destination, a.id_periode, a.id_hebergement, t.db_date) as qte_resa
        from `vac_association_cata2` a inner join (`vac_periodes2` p inner join `time_dimension` t on t.db_date between p.date_debut and p.date_fin) on p.id = a.id_periode
        where t.db_date between pdate_debut and pdate_fin and a.id_destination = pid_destination
        group by t.db_date, a.id_catalogue, a.id_type_destination, a.id_destination, a.id_periode, a.id_hebergement
        order by t.db_date, a.id_periode, a.id_hebergement; 
    else
        if pid_hebergement = 0 then
            select t.db_date, a.id_catalogue, a.id_type_destination, a.id_destination, a.id_periode, a.id_hebergement,
            date_format(t.db_date, '%d/%m/%Y') as db_date_fr, dayofweek(t.db_date) as day, a.qte as qte_org,
            `wp_get_dispo_func`(a.id_catalogue, a.id_type_destination, a.id_destination, a.id_periode, a.id_hebergement, t.db_date) as qte_resa
            from `vac_association_cata2` a inner join (`vac_periodes2` p inner join `time_dimension` t on t.db_date between p.date_debut and p.date_fin) on p.id = a.id_periode
            where t.db_date between pdate_debut and pdate_fin and a.id_destination = pid_destination and a.id_periode = pid_periode
            group by t.db_date, a.id_catalogue, a.id_type_destination, a.id_destination, a.id_periode, a.id_hebergement
            order by t.db_date, a.id_periode, a.id_hebergement; 
        else if pid_periode = 0 then
            select t.db_date, a.id_catalogue, a.id_type_destination, a.id_destination, a.id_periode, a.id_hebergement,
            date_format(t.db_date, '%d/%m/%Y') as db_date_fr, dayofweek(t.db_date) as day, a.qte as qte_org,
            `wp_get_dispo_func`(a.id_catalogue, a.id_type_destination, a.id_destination, a.id_periode, a.id_hebergement, t.db_date) as qte_resa,
            `wp_get_attribution_func`(a.id, t.db_date) as qte_attribution,
            `get_web_reservation_func`(a.id, t.db_date) as qte_web_resa            
            from `vac_association_cata2` a inner join (`vac_periodes2` p inner join `time_dimension` t on  p.date_debut <= t.db_date and t.db_date < p.date_fin) on p.id = a.id_periode
            where t.db_date between pdate_debut and pdate_fin and a.id_destination = pid_destination
            and a.id_hebergement = pid_hebergement
            group by t.db_date, a.id_catalogue, a.id_type_destination, a.id_destination, a.id_periode, a.id_hebergement
            order by t.db_date, a.id_periode, a.id_hebergement;         
        else 
            select t.db_date, a.id_catalogue, a.id_type_destination, a.id_destination, a.id_periode, a.id_hebergement,
            date_format(t.db_date, '%d/%m/%Y') as db_date_fr, dayofweek(t.db_date) as day, a.qte as qte_org,
            `wp_get_dispo_func`(a.id_catalogue, a.id_type_destination, a.id_destination, a.id_periode, a.id_hebergement, t.db_date) as qte_resa,
            `wp_get_attribution_func`(a.id, t.db_date) as qte_attribution
            from `vac_association_cata2` a inner join (`vac_periodes2` p inner join `time_dimension` t on  p.date_debut <= t.db_date and t.db_date < p.date_fin) on p.id = a.id_periode
            where a.id_destination = pid_destination and a.id_periode = pid_periode
            and a.id_hebergement = pid_hebergement
            group by t.db_date, a.id_catalogue, a.id_type_destination, a.id_destination, a.id_periode, a.id_hebergement
            order by t.db_date, a.id_periode, a.id_hebergement;             
        end if;         
        end if;
    end if;
END$$

DELIMITER ;

