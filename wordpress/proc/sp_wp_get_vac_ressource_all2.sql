CREATE DEFINER=`sa`@`%` PROCEDURE `sp_wp_get_vac_ressource_all2`(id_desti INT(11), p_semaine VARCHAR(10))
BEGIN
    SELECT distinct(h.id) as type_hebergement,
    IF(ta.tarif_btd_location=0,0,1) as logement,
    max(ta.tarif_btd_pc) as px_pension_cplete,
    max(ta.tarif_btd_dp) as px_demi_pension,
    max(ta.tarif_btd_sp) as px_sanspens,
    IFNULL(at.tx_reduction,0) as tx_reduction,    
    h.libelle as libelle,
    IFNULL(c.annee,0) as annee,
    ac.hautesaison as hautesaison
    FROM p_hebergement2 h
    INNER JOIN vac_association_cata2 ac ON ac.id_hebergement=h.id and ac.aff_web=1
    INNER JOIN vac_periodes2 p ON p.id=ac.id_periode
    INNER JOIN vac_tarifs_adultes2 ta ON ta.id=ac.id_tarif_adulte
    LEFT JOIN vac_association_tenf2 at ON at.id_association=ac.id
    LEFT JOIN vac_catalogue2 c ON c.id=ac.id_catalogue
    WHERE ac.id_destination=id_desti AND NOW()<p.date_fin
    GROUP BY h.id
    ORDER BY h.libelle;
END