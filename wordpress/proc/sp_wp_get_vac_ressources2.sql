CREATE DEFINER=`sa`@`%` PROCEDURE `sp_wp_get_vac_ressources2`(id_desti INT(11), p_catalogue VARCHAR(10), id_heberg INT(11), p_semaine VARCHAR(10), date_debut DATE, date_fin DATE)
BEGIN
    SELECT ac.id,
        DATE_FORMAT(p.date_debut,'%d/%m/%y') as debut, 
        DATE_FORMAT( p.date_fin,'%d/%m/%y') as fin,
        ta.tarif_btd_location as px_don_dro,
        ta.tarif_ref_location as px_exterieur,
        ta.tarif_ref_pc as px_pension_cplete_e, 
        ta.tarif_btd_pc as px_pension_cplete_f, 
        ta.tarif_ref_dp as px_demi_pension_e,
        ta.tarif_btd_dp as px_demi_pension_f,
        ta.tarif_ref_sp as px_sanspens_e, 
        ta.tarif_btd_sp as px_sanspens_f, 
        (get_tarif_enfant_func2(ac.id, 0, "MOIS", 0,3)) as tarif_enfant_0_3_f,
        (get_tarif_enfant_func2(ac.id, 0, "MOIS", 3,36)) as tarif_enfant_3_3_f,
        (get_tarif_enfant_func2(ac.id, 0, "ANNEE", 3,8)) as tarif_enfant_3_8_f,
        (get_tarif_enfant_func2(ac.id, 0, "ANNEE", 8,14)) as tarif_enfant_8_14_f,
        (get_tarif_enfant_func2(ac.id, 0, "ANNEE", 14, 18)) as tarif_enfant_14_18_f,
        (get_tarif_enfant_func2(ac.id, 1, "MOIS", 0,3)) as tarif_enfant_0_3_e,
        (get_tarif_enfant_func2(ac.id, 1, "MOIS", 3,36)) as tarif_enfant_3_3_e,
        (get_tarif_enfant_func2(ac.id, 1, "ANNEE", 3,8)) as tarif_enfant_3_8_e,
        (get_tarif_enfant_func2(ac.id, 1, "ANNEE", 8,14)) as tarif_enfant_8_14_e,
        (get_tarif_enfant_func2(ac.id, 1, "ANNEE", 14, 18)) as tarif_enfant_14_18_e,
        (get_dispo_pourcentage_func2(ac.id_destination, ac.id_hebergement, ac.id_periode)) as dispo,
        ac.hautesaison as selection,
        p.id as periode
    FROM vac_destination2 d
    INNER JOIN vac_association_cata2 ac ON ac.id_destination=d.id and ac.aff_web=1
    INNER JOIN vac_catalogue2 c ON c.id=ac.id_catalogue
    INNER JOIN vac_periodes2 p ON p.id=ac.id_periode
    INNER JOIN vac_tarifs_adultes2 ta ON ta.id=ac.id_tarif_adulte
    WHERE ac.id_destination=id_desti and ac.id_hebergement=id_heberg 
    AND NOW()<p.date_fin
    ORDER BY p.date_debut;
END