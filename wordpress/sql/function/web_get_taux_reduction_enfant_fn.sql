CREATE DEFINER=`sa`@`%` FUNCTION `web_get_taux_reduction_enfant`(
    pid_asso int(11),
    pdatenais date,
    fact_debut_periode date,
    pexterieur bit(1)
) RETURNS int(11)
BEGIN

    declare seof smallint(1) default 0;
    declare mt_fact decimal(19, 2) default 0;
    declare age smallint(4) default `calcul_age_func2`(pdatenais, fact_debut_periode);
    
    declare te_id int(11);
    declare te_tranche_inferieure smallint(6);
    declare te_tranche_superieure smallint(6);
    declare te_unite varchar(5);
    
    declare tenf cursor for select vte.id, vte.tranche_inferieure, vte.tranche_superieure, vte.unite from `vac_association_tenf2` vat 
    inner join `vac_tarifs_enfants2` vte on vte.id = vat.id_tarif_enfant 
    where vat.id_association = pid_asso and vte.exterieur = pexterieur;

    declare continue handler for sqlstate '02000' set seof = 1;

get_tranche_tarif_enf : begin
    open tenf;
    boucle_tenf : loop
        fetch tenf into te_id, te_tranche_inferieure, te_tranche_superieure, te_unite;
        if seof = 1 then
            leave boucle_tenf;
        end if;
        if `Appartient_a_tranche_func3`(pdatenais, fact_debut_periode, te_tranche_inferieure, te_tranche_superieure, te_unite) = true then
            leave get_tranche_tarif_enf;
        end if;
    end loop boucle_tenf;
    close tenf;
    set te_id = 0;
end get_tranche_tarif_enf;
if te_id <> 0 then
    SELECT tx_reduction into te_id
    FROM vac_association_tenf2    
    WHERE id_association= pid_asso and id_tarif_enfant= te_id;
END IF;
return te_id;
end